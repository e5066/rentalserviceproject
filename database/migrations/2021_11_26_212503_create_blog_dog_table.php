<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogDogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_dog', function (Blueprint $table) {
            $table->primary(['dog_id','blog_id']);
            $table->bigInteger('dog_id')->unsigned();
            $table->bigInteger('blog_id')->unsigned();
            $table->foreign('dog_id')->references('id')->on('dogs')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('blog_id')->references('id')->on('blogs')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_dog');
    }
}
