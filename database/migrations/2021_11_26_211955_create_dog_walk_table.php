<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDogWalkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dog_walk', function (Blueprint $table) {
            $table->primary(['dog_id','walk_id']);
            $table->boolean('approve');
            $table->bigInteger('dog_id')->unsigned();
            $table->bigInteger('walk_id')->unsigned();
            $table->timestamps();
            $table->foreign('dog_id')->references('id')->on('dogs')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('walk_id')->references('id')->on('walks')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dog_walk');
    }
}
