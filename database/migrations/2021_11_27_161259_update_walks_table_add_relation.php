<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWalksTableAddRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('walks', function (Blueprint $table) {
            $table->bigInteger('timeslot_id')->unsigned();
            $table->foreign('timeslot_id')->references('id')->on('timeslots')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('walks', function (Blueprint $table) {
            $table->dropColumn('timeslot_id');
            $table->dropForeign('timeslot_id');
        });
    }
}
