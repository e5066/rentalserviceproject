import i from "../../public/js/apexcharts/apexcharts.esm";

var url = window.location.pathname

if (url == '/pending') {
    window.onload = function () {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const id = urlParams.get('id')

        const changeOrderStatus = function (status) {
            const body = document.getElementById('pending-body-text')
            const header = document.getElementById('pending-header-text')
            document.getElementById('pending-spinner').style = 'display: none';
            header.style = 'color: white'
            if (status == 'failed') {
                body.innerHTML = 'Er is wat misgegaan bij het betalen, probeer later opnieuw'
                header.parentElement.style = 'background-color: crimson'
                header.innerHTML = 'Betaling mislukt'
                body.parentElement.style = 'border: 2px solid crimson'
            } else if (status == 'canceled') {
                body.innerHTML = 'U heeft de betaling geannuleerd'
                header.parentElement.style = 'background-color: crimson'
                header.innerHTML = 'Betaling geannuleerd'
                body.parentElement.style = 'border: 2px solid crimson'
            } else if (status == 'paid') {
                body.innerHTML = 'Uw betaling is verwerkt, Bedankt voor uw aankoop'
                header.parentElement.style = 'background-color: seagreen'
                body.parentElement.style = 'border: 2px solid seagreen'
                header.innerHTML = 'Betaling gelukt'
            } else if (status == 'expired') {
                body.innerHTML = 'Uw betaling is verlopen, probeer opnieuw'
                header.parentElement.style = 'background-color: crimson'
                body.parentElement.style = 'border: 2px solid crimson'
                header.innerHTML = 'Betaling verlopen'
            }
        }

        var intervalId = window.setInterval(function(){
            fetch('/check-status/' + id)
                .then(response => response.json())
                .then (data => {
                    if (data.status !== 'open') {
                        clearInterval(intervalId)
                        changeOrderStatus(data.status)
                    }
                })
                 ;
        }, 5000);
    }

}
