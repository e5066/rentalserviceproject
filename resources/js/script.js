const tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate()+1);
var url = window.location.pathname

if (url == "/walk") {
    window.onload = function () {

        let dogsLoaded = false
        let timeslotsLoaded = false
        let boughtWalksLoaded = false
        let dataLoaded = false

        let minDate = new Date()
        minDate.setDate(minDate.getDate() - 1)

        let walksBought = 0
        let selectedDate = null
        let amountOfDogs = 0
        let filterTimespots = []

        let availableDogs = []
        let chosenDogs = []

        let availableTimezones = []
        let timezonesSelect = []
        let data = []

        let maxDate = new Date()

        let fullDates = [minDate, maxDate]

        let selectedTimeslot = null

        const changeFullDates = function() {
            fullDates = [minDate, maxDate]
            data.forEach((item, key) => {
                let full = true

                for (const [key, value] of Object.entries(item.timeslots)) {
                    if (value + amountOfDogs <= 8) {
                        full = false
                    }
                }

                if (filterTimespots.length > 0) {
                    full = true
                    let hasOptionAvailable = false
                    filterTimespots.forEach((filterTimezone, key) => {
                        for (const [key, value] of Object.entries(item.timeslots)) {
                            if (value + amountOfDogs <= 8 && filterTimezone === key) {
                                hasOptionAvailable = true
                            }
                        }
                    })

                    if (hasOptionAvailable) {
                        full = false
                    }

                }

                if (full) {
                    fullDates.push(new Date(item.date))
                }
            })
        }

        const timeslotInput = new Choices(document.getElementById('timeslotInput'),
            { removeItemButton: true, placeholder: true, placeholderValue: 'Kies uw tijd'}
        );
        timeslotInput.disable()

        const changeTimeslot = function(date, enabled) {
            timezonesSelect.forEach((item, key) => {
                if (timezonesSelect[key].value == date) {
                    timezonesSelect[key].disabled = !enabled
                }
            })
        }

        const changeTimeslotsInput = function(date) {
            selectedDate = date

            data.forEach((item, key) => {
                let comparableDate = selectedDate.toISOString().split("T")[0]
                if (item.date === comparableDate) {
                    for (const [key, value] of Object.entries(item.timeslots)) {
                        if (value + amountOfDogs <= 8) {
                            changeTimeslot(key, true)
                        } else {
                            changeTimeslot(key, false)
                        }
                    }
                }
            })

            timeslotInput.setChoices(
                timezonesSelect,
                'value',
                'label',
                true
            )

        }

        const datePickerOptions = {
            display: {
                inline: true,
                components: {
                    decades: false,
                    year: true,
                    month: true,
                    date: true,
                    hours: false,
                    minutes: false,
                    seconds: false,
                }
            },
            useCurrent: false
        }

        const datePicker = new tempusDominus.TempusDominus(document.getElementById('inlinePicker'), datePickerOptions);
        datePicker.hide()

        const subscription = datePicker.subscribe(tempusDominus.Namespace.events.change, (e) => {
            changeTimeslotsInput(e.date)
            timeslotInput.enable()
        });

        const filterTimespotsInput = new Choices(document.getElementById('nameInput'),
            {
                removeItemButton: true,
                placeholder: true,
                placeholderValue: 'Voorgekeurde tijden zoeken',
                itemSelectText: "Klik om te kiezen",
            }
        );

        const dogsInput = new Choices(document.getElementById('honden'),
            {
                removeItemButton: true,
                placeholder: true,
                placeholderValue: 'Kies uw honden',
                maxItemText: "U heeft niet genoeg in uw stripkaart om meer honden toe te voegen",
                itemSelectText: "Klik om te kiezen",
            }
        );

        dogsInput.passedElement.element.addEventListener(
            'change',
            function(event) {
                chosenDogs = dogsInput.getValue(true)
                amountOfDogs = chosenDogs.length
                if (amountOfDogs > 0) {
                    datePicker.show()
                    document.getElementById('chooseDateLabel').style = 'display: block'
                } else {
                    datePicker.hide()
                    document.getElementById('chooseDateLabel').style = 'display: none'
                    datePicker.clear()
                    timeslotInput.disable()
                    document.getElementById('submitCreateWalk').disabled = true
                }


                changeFullDates()
                datePicker.updateOptions(
                    {
                        restrictions: {
                            disabledDates: fullDates
                        }
                    }
                )
            },
            false,
        );

        filterTimespotsInput.passedElement.element.addEventListener(
            'change',
            function(event) {
                filterTimespots = filterTimespotsInput.getValue(true)
                changeFullDates()
                datePicker.updateOptions(
                    {
                        restrictions: {
                            disabledDates: fullDates
                        }
                    }
                )
            },
            false,
        );

        timeslotInput.passedElement.element.addEventListener(
            'change',
            function(event) {
                selectedTimeslot = event.detail.value

                if (event.detail.value) {
                    document.getElementById('submitCreateWalk').disabled = false
                } else {
                    document.getElementById('submitCreateWalk').disabled = false
                }
            },
            false,
        );

        fetch('/user/dogs')
            .then(response => response.json())
            .then(data => {
                availableDogs = data;
                dogsLoaded = true
                enablePage()
            } );

        fetch('/timeslots')
            .then(response => response.json())
            .then(data => {
                availableTimezones = data;
                timezonesSelect = data;
                timeslotsLoaded = true;
                enablePage()
            } );

        fetch('/walks-available')
            .then(response => response.json())
            .then(data => {
                walksBought = data
                boughtWalksLoaded = true
                enablePage()
            } );

        fetch('/walks-per-month')
            .then(response => response.json())
            .then(responseData => {
                data = responseData
                dataLoaded = true
                enablePage()
            } );

        const enablePage = function() {
            if (dogsLoaded && timeslotsLoaded && boughtWalksLoaded && dataLoaded) {
                maxDate = new Date(data[data.length - 1].date)
                maxDate.setDate(maxDate.getDate() + 1)
                fullDates = [minDate, maxDate]
                changeFullDates()

                datePicker.updateOptions({
                    restrictions: {
                        minDate: minDate,
                        maxDate: maxDate,
                        disabledDates: fullDates
                    }
                })

                dogsInput.config.maxItemCount = walksBought

                dogsInput.setChoices(
                    availableDogs,
                    'id',
                    'name',
                    false
                );

                filterTimespotsInput.setChoices(
                    availableTimezones,
                    'value',
                    'label',
                    false,
                );

                document.getElementById('submitCreateWalk').addEventListener('click', function(event) {
                    const response = fetch('/create-walk', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': document.getElementsByName("_token")[0].value
                        },
                        body: JSON.stringify({
                            date: selectedDate.toISOString().split('T')[0],
                            time: selectedTimeslot,
                            dogs: chosenDogs
                        })
                    })
                        .then(response => response.json())
                        .then(data => {
                            document.getElementById('create-walk-form').style = 'display: none';
                           if (data.status == 'success') {
                               document.getElementById('create-walk-success').style = 'display: block';
                           } else {
                               document.getElementById('create-walk-failed').style = 'display: block';
                           }
                        } );
                })

            }
        }

    }

}
