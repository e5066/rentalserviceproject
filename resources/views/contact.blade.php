@extends('layouts.app')

@section('content')
    <head>
        <link href="{{asset('css/contactpage.css')}}" rel="stylesheet">
    </head>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <class>
                <img src="{{asset('images/logo.png')}}" style="width:128px;height:128px;">
            </class>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card" >
                <div class="row">
                    <div class="col-md-8 mb-5">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3205.124725506973!2d5.219144693771732!3d52.36940344870029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c616e13959b029%3A0xe04f21626e2f3f0e!2sWindesheim%20in%20Almere!5e0!3m2!1snl!2snl!4v1641937245877!5m2!1snl!2snl" width="100%" height="550" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('message_sent'))
                                    <div class="alert alert-success" id="alert">
                                        <div class="row">
                                            <div class="col-md-10">
                                                {{Session::get('message_sent')}}
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="close justify-content-end" data-bs-dismiss="alert"><i class="contact-close-btn bi-x"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                    <div class="h4 text-center">Contactformulier</div>

                                <form method="POST" action="{{route('contact.send')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group mt-3">
                                        <input type="text" name="name" class="form-control" placeholder="Volledige naam" />
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group mt-3">
                                        <input type="text" name="email" class="form-control" placeholder="Email" />
                                        @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group mt-3">
                                        <input type="text" name="phone" class="form-control" placeholder="Telefoonnummer"/>
                                        @error('phone')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group mt-3">
                                        <input type="text" name="subject" class="form-control" placeholder="Onderwerp"/>
                                        @error('subject')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group mt-3">
                                        <textarea name="msg" class="form-control" rows="5" placeholder="Uw vraag a.u.b."></textarea>
                                        @error('msg')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group mt-3 mb-3 justify-content-center text-center">
                                        <input type="submit" class= "contact-submit-btn btn-primary justify-content-center text-center" value="Verzenden">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


