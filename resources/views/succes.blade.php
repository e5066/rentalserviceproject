@extends('layouts.app')

@section('content')
    <div class="pending">
        <div class="card">
            <div class="card-header">
                <p id="pending-header-text">Betaling wordt verwerkt</p>
                <div id="pending-spinner" class="spinner-border" role="status">
                </div>
            </div>
            <div class="card-body">
                <p id="pending-body-text">Uw betaling wordt verwerkt</p>
            </div>
        </div>
    </div>
@endsection
