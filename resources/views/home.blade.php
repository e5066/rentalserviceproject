@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-grid gap-3"><div class="p-5"></div></div>
        <div class="d-grid gap-3"><div class="p-5"></div></div>
        <div class="row justify-content-evenly align-items-center">
            <div class="col-12 col-lg-6 text-lg-start text-center">
                <h1 id="h1Title">Jamy's <br>Uitlaatservice</h1>
                <h3 id="h3sub">Enkele en groepswandelingen<br>
                    voor honden in de omgeving Lelystad</h3>
            </div>
            <div class="col-12 col-lg-6 text-center text-lg-end pt-5 pt-lg-0">
                <a id="goToShop" class="p-3 p-lg-4" href="{{url('shop')}}">Boek nu uw wandeling!</a>
            </div>
        </div>
    </div>

    <style>
        .bgStandard{
            background-image: url("{{asset('img/bgHome.png')}}");
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        #h1Title{
            font-family: "Montserrat", sans-serif;
            font-size: 8vh;
            color: white;
        }
        #h3sub{
            font-family: "Montserrat", sans-serif;
            font-size: 2.5vh;
            color: #51BFA6;
        }
        #goToShop{
            background-color: #51BFA6;
            color: white;
            border: 2px solid #51BFA6;
            border-radius: 55px;
            font-family: "Montserrat", sans-serif;
            font-size: 3vh;
            text-decoration: none;
            font-weight: bold;
        }
        @media only screen and (max-width: 768px) {
            #goToShop{
                font-size: 2vh;
            }
            #h1Title{
                font-size: 3.5vh;
            }
            #h3sub{
                font-size: 2vh;
            }
        }
    </style>
@endsection
