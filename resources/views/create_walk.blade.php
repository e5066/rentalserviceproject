@extends('layouts.app')

@section('content')

    <div id="create-walk-success">
        <h1>Uw aanvraag is succesvol</h1>
    </div>
    <div id="create-walk-failed">
        <h1>Uw aanvraag is mislukt. Probeer later opnieuw</h1>
    </div>
    <div id="create-walk-form" class="checkout create-walk">
        <div class="checkout-form">
            <h1 class="h1">Aanvraag formulier</h1>
            <select multiple id="honden"></select>
            <select multiple id="nameInput"></select>

            <h2>Tijdsloten</h2>
            @csrf
            <select id="timeslotInput"></select>
            <button id="submitCreateWalk" class="btn btn-primary" disabled>Aanvragen</button>
        </div>

        <div class="product">
            <h3 id="chooseDateLabel" style="display: none">Kies uw datum</h3>
            <div class="body">
                <div class='log-event' id='inlinePicker'></div>
            </div>
        </div>
    </div>

@endsection
