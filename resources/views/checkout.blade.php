@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
        <form method="post" action="{{action('ProductsController@finalizeCheckout', $product->id)}}">
            @csrf
            <input type="hidden" name="product" value="{{$product->id}}">
            <div class="form-group">
                <label>Voornaam</label>
            <input class="form-control" name="firstname" type="text">
            </div>
            <div class="form-group">
                <label>Achternaam</label>
                <input class="form-control" name="lastname" type="text">
            </div>
            <div class="form-group">
                <label>Adres</label>
                <input class="form-control" name="address" type="text">
            </div>
            <div class="form-group">
                <label>Huisnummer</label>
                <input class="form-control" name="houseNo" type="text">
            </div>
            <div class="form-group">
                <label>Postcode</label>
                <input class="form-control" name="postcode" type="text">
            </div>
            <div class="form-group">
                <label>Betaalwijze</label>
            <select class="form-select">
                <option>iDeal</option>
                <option>Paypal</option>
            </select>
            </div>
            <div class="form-group">
                <input class="button" value="Betaling afronden" type="submit">
            </div>
        </form>
        </div>

        <div class="product">
            <div class="header">
                <div class="price">
                    € {{$product->price}}
                </div>
            </div>
            <div class="body">
                <div class="product-image">
                    <img src="{{asset('img/' . $product->img)}}" alt="strippenkaart">
                </div>
                <div class="product-text">
                    <h5>{{$product->name}}</h5>
                    <p>{{$product->description}}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
