@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ Auth::user()->name }}</div>
            </div>
        </div>
    </div>
</div>--}}

<section id="hero" class="d-flex align-items-center">

<div class="container" data-aos="zoom-out" data-aos-delay="100">
    <div class="row">
        <div class="col-xl-6">
            <h1>Jamy's </h1>
            <h1>Uitlaatservice </h1>
            <h2 class="text-success">Enkele en groepswandelingen</h2>
            <h2 class="text-success">voor honden in de omgeving Lelystad </h2>
            <a href="#about" class="btn-get-started navbar-nav-scroll bg-success ">Boek nu uw wandeling!</a>
        </div>
    </div>
</div>
</div>

</section><!--end Home section -->

<main id="main">


@endsection
