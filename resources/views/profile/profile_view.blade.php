@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 20%">
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile[0]->id.'/honden') }}">Bekijk Honden</a>
                </div>
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile[0]->id.'/edit') }}">Verander Gegevens</a>
                </div>
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile[0]->id.'/kaarten') }}">Bekijk Strippenkaarten</a>
                </div>
            </div>
            <div class="" style="float: right; width:75%">
                <div class="row">
                    <h3>Gebruikersnaam:</h3>
                    <h5>{{$profile[0]->name}}</h5>
                    <h3>Naam:</h3>
                    <h5>{{$profile[0]->firstname}} {{$profile[0]->lastname}}</h5>
                    <h3>Email:</h3>
                    <h5>{{$profile[0]->email}}</h5>
                </div>
                <br>
                <br>
            </div>

        </div>
    </div>

@endsection
