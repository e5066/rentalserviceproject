@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 20%">
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile)}}">Terug naar profiel</a>
                </div>
                <div class="row">
                    <a class="button" href="{{ url('')}}">Koop Nieuwe Strippenkaart</a>
                </div>
            </div>
            <div class="" style="float: right; width:75%">
                <div class="form-group">
                    <h2>Uw Kaarten:</h2>
                </div>
                <div class="form-group">
                    @foreach($kaarten as $kaart)
                        <div class="form-group">
                            <h3>{{$kaart->name}}</h3>
                            <h5>Totaal: {{$kaart->total}}</h5>
                            <h5>Gebruikt: {{$kaart->used}}</h5>
                        </div>
                    @endforeach
                    <br>
                </div>
            </div>
        </div>
    </div>

@endsection
