@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 20%">
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile[0]->id . '/honden')}}">Toevoegen Annuleren</a>
                </div>
            </div>
            <div class="" style="float: right; width:75%">
                <form method="post" action="{{url('profile/' . $profile[0]->id . '/honden')}}">
                    @csrf
                    <div class="form-group">
                        <h2>Hond toevoegen</h2>
                    </div>
                    <div class="form-group">
                        <label>Naam:</label>
                        <input class="form-control" name="ownerid" type="hidden" value="{{$profile[0]->id}}">
                        <input class="form-control" name="name" type="text">
                    </div>
                    <div class="form-group">
                        <label>Toestaan op blog:</label>
                        <select class="form-control" id="blogallow" name="blogallow">
                            <option value="1" selected>Ja</option>
                            <option value="0">Nee</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="button button-fullwidth" value="Hond Toevoegen" type="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
