@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 20%">
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile[0]->id . '/honden')}}">Aanpassingen Annuleren</a>
                </div>
            </div>
            <div class="" style="float: right; width:75%">
            <form method="post" action="{{url('profile/' . $profile[0]->id . '/' . $dog[0]->id)}}">
                @csrf
                @method('patch')
                <div class="form-group">
                    <label>Naam:</label>
                    <input class="form-control" name="updid" type="hidden" value="{{$dog[0]->id}}">
                    <input class="form-control" name="retid" type="hidden" value="{{$profile[0]->id}}">
                    <input class="form-control" name="name" type="text" value="{{$dog[0]->name}}">
                </div>
                <div class="form-group">
                    <label>Toestaan op blog:</label>
                    <select class="form-control" id="blogallow" name="blogallow">
                        <option value="1" >Ja</option>
                        <option value="0"
                        @if ($dog[0]->permissionPhotos == 0)
                            selected
                        @endif
                        >Nee</option>
                    </select>
                </div>
                <div class="form-group">
                    <input class="button button-fullwidth" value="Hond Aanpassen" type="submit">
                </div>
            </form>

                <form method="post" action="{{url('/profile/' . $profile[0]->id . '/' . $dog[0]->id)}}">
                    @csrf
                    @method('delete')
                    <div class="form-group">
                        <input class="form-control" type="hidden" id="retid" value="{{$profile[0]->id}}">
                        <input class="form-control" type="hidden" id="delid" name="delid" value="{{$dog[0]->id}}">
                    </div>
                    <div class="form-group">
                        <input class="button" style="background-color: red;" type="submit" value="Verwijderen">
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection
