@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 20%">
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile)}}">Terug naar profiel</a>
                </div>
                <div class="row">
                    <a class="button" href="{{ url('profile/'.$profile . '/honden/create')}}">Voeg nieuwe hond toe</a>
                </div>
            </div>
            <div class="" style="float: right; width:75%">
                <div class="form-group">
                    <h2>Uw honden:</h2>
                </div>
                @foreach($dogs as $dog)
                    <h5>{{$dog->name}}</h5>
                    <a class="button" style="margin-top: 20px;margin-bottom:  20px" href="{{ url('profile/' . $profile . '/' . $dog->id . '/edit') }}">Aanpassen</a>
                    <br>
                    <br>
                @endforeach
                <br>
            </div>
        </div>
    </div>

@endsection
