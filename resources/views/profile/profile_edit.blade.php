@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 20%">
                <div class="row">
                    <a class="button form-control" href="{{ url('profile/'.$profile[0]->id)}}">Aanpassingen Annuleren</a>
                </div>
                <div class="row">
                    <a class="button form-control" href="{{ url('profile/'.$profile[0]->id.'/editpassword') }}">Verander Wachtwoord</a>
                </div>
            </div>
            <div class="" style="float: right; width:75%">
                <form method="post" action="{{url('profile/' . $profile[0]->id)}}">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Gebruikersnaam</label>
                        <input class="form-control" name="updid" type="hidden" value="{{$profile[0]->id}}">
                        <h3>{{$profile[0]->name}}</h3>
                    </div>
                    <div class="form-group">
                        <label>Voornaam</label>
                        <input class="form-control" name="firstname" type="text" value="{{$profile[0]->firstname}}">
                    </div>
                    <div class="form-group">
                        <label>Achternaam</label>
                        <input class="form-control" name="lastname" type="text" value="{{$profile[0]->lastname}}">
                    </div>
                    <div class="form-group">
                        <label>E-Mail</label>
                        <input class="form-control" name="email" type="text" value="{{$profile[0]->email}}">
                    </div>
                    <div class="form-group">
                        <input class="button button-fullwidth" value="Profiel Aanpassen" type="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
