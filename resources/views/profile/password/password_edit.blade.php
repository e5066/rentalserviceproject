@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 20%">
                <a class="button" href="{{ url('profile/'.$profile[0]->id . '/edit')}}">Aanpassing Annuleren</a>
            </div>
            <div class="" style="float: right; width:75%">
                <form method="post" action="{{url('profile/' . $profile[0]->id)}} . '/password'">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Gebruikersnaam</label>
                        <input class="form-control" name="updid" type="hidden" value="{{$profile[0]->id}}">
                        <h3>{{$profile[0]->name}}</h3>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" name="firstname" type="text" value="">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input class="form-control" name="lastname" type="text" value="">
                    </div>
                    <div class="form-group">
                        <input class="button button-fullwidth" value="Wachtwoord Aanpassen" type="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
