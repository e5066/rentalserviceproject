@extends('layouts.app')

@section('content')
    <div class="main">
        @foreach($products as $product)
        <div class="product">
            <div class="header">
                <div class="price">
                    € {{$product->price}}
                </div>
            </div>
            <div class="body">
                <div class="product-image">
                    <img src="{{asset('img/' . $product->img)}}" alt="strippenkaart">
                </div>
                <div class="product-text">
                    <h5>{{ $product->name }}</h5>
                    <p> {{$product->description}}</p>
                    <a href="checkout/{{$product->id}}" class="button">Bestellen</a>
                </div>
            </div>
        </div>

        @endforeach
    </div>
@endsection
