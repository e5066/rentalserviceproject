@extends('layouts.app')
<head>
    <link rel="stylesheet" href="{{asset('css/blog.css')}}"/>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@section('content')
    <div class="container-fluid blog-bg">
        <div class="container py-4">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6" id="post">
                    @foreach($pagination as $blog)
                        <div class="card mb-5 mt-3 blog-card">
                            <div class="card-body px-0 pt-0">
                                <img class="img-fluid" style="width: 100%" src="data:image;base64,{{base64_encode($blog->photo)}}" onclick="likeClick({{$blog->id}},'{{$blog->id . 'lc' . $blog->likeCount}}')" ondblclick="likeClick({{$blog->id}},'{{$blog->id . 'lc' . $blog->likeCount}}')"/>
                                <div class="row align-items-center justify-content-start px-3 pt-1">
                                    <div class="col-auto align-self-center my-auto pe-2 ps-0">
                                        <p class="mb-1" id="{{$blog->id}}lc{{$blog->likeCount}}">{{$blog->likeCount}}</p>
                                    </div>
                                    <div class="col-auto align-self-center pe-2 ps-0">
                                        <i id="{{$blog->id}}" class="bi {{($blog->liked == 1 ? 'bi-heart-fill' : 'bi-heart')}}" style="font-size: 16px; {{($blog->liked == 1 ? 'color: red;' : 'color: black;')}}"></i>
                                    </div>
                                    <div class="col-auto align-self-center pe-2 ps-0">
                                        <p class="text-start mb-1">{{date_format(date_create($blog->created_at), 'd-m-Y')}}</p>
                                    </div>
                                </div>
                                <div class="row px-3 justify-content-start">
                                    <div class="col-12 p-0">
                                        <p>{{$blog->text}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{-- here loads posts --}}
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-6 text-center">
                    <div class="spinner-border" role="status">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/create_blog_items.js')}}"></script>
    </script>
@endsection
