<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Bericht</title>
</head>
<body>
    <h3><p>Onderwerp: {{$details['subject']}}</h3>
    <p>Naam: {{$details['name']}}</p>
    <p>Email: {{$details['email']}}</p>
    <p>Phone: {{$details['phone']}}</p>

    <p>Bericht van {{$details['name']}}:</p><br>
    <p>{{$details['msg']}}</p>
</body>
</html>
