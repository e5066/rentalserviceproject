<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>


<p>Beste {{$details['firstname']}},</p>
<p>De afspraak voor uw hond {{$details['dogName']}} met onderstaande gegevens. Is helaas geannuleerd.</p>
<p>Geselecteerde hond: {{$details['dogName']}}</p>
<p>Tijdslot: {{$details['timeSlot']}}</p>
<p>Datum: {{$details['walkDate']}}</p>
<p>Klantnaam: {{$details['firstname']}}</p>
<p>U kunt een verzoek indienen voor een nieuwe afspraak. Neem a.u.b. contact met ons op.</p>
<p>Mvg,<br>Jamy</p>
</body>
</html>
