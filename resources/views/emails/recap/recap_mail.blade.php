<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Uw hond: {{$details['selectedDog']}}</h1>
    <h4>Terugkoppeling wandeling:</h4>
    <p>{{$details['msg']}}</p>

    <p>Mvg,<br>Jamy</p>
</body>
</html>
