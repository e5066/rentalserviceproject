@extends('layouts.app')

@section('content')
    <div class="container pt-5 pb-5 ">

        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="card loginContainer border-dark shadow ">
                    <div class="card-body border-dark shadow-lg ">
                        <div class="card-header text-center bg-transparent border-light"><img src="{{URL::asset('images/logo.png')}}"  width="15%" height="auto"/></div>

                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row justify-content-center">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input id="email" type="email" placeholder="email" class="input-fields form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        <label for="email">Email address</label>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row justify-content-center">
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input id="password" type="password" placeholder="password" class="input-fields form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                        <label for="password">Password</label>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row justify-content-center">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input remember-check" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="input-fields form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-6 justify-content-center">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-success loginBtn m-5 me-4 ps-xl-5 pe-5 pt-1 pb-1 mb-4">
                                        {{ __('Login') }}
                                    </button>
                                    <div class="col-md-12 text-center">
                                    <a href="register" class="btn btn-transparent loginBtn m-3 ps-5 pe-5 pt-1 pb-1 mb-4">Registreren</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row m-1 justify-content-end float-right">
                                <div class="col-md-12 text-center m-2">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link btn-reset mb-0" href="{{ route('password.request') }}">
                                            {{ __('Wachtwoord vergeten?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
