@extends('layouts.app')
@section('title', 'Register')
@section('content')
    <head>
        <link rel="stylesheet" href="{{asset('css/register.css')}}">
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <div class="container px-5 py-3">
        <div class="row px-3 register-card justify-content-center">
            <div class="row justify-content-center">
                <img src="{{asset('assets/img/favicon.png')}}" style="max-width: 20vh" class="img-fluid"/>
            </div>
            <div class="row">
                <div class="col-12 col-lg-7 py-3" id="userFields">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6">
                            <div class="form-floating mb-3">
                                <input id="firstname" type="text" placeholder="Voornaam" class="border-success input-fields form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                                <label for="firstname"> Voornaam </label>
                                @error('firstname')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating mb-3">
                                <input id="lastname" type="text" placeholder="Achternaam" class="border-success input-fields form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
                                <label for="lastname"> Achternaam </label>
                                @error('lastname')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6">
                            <div class="form-floating mb-3">
                                <input id="street" type="text" placeholder="Straat" class="border-success input-fields form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" required autocomplete="street" autofocus>
                                <label for="street"> Straat </label>
                                @error('street')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-5 col-md-3 pe-0 pe-md-3">
                            <div class="form-floating mb-3">
                                <input id="home_no" type="text" placeholder="Huisnr" class="border-success input-fields form-control @error('home_no') is-invalid @enderror" name="home_no" value="{{ old('home_no') }}" required autocomplete="home_no" autofocus>
                                <label for="home_no"> Huisnr </label>
                                @error('home_no')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-7 col-md-3">
                            <div class="form-floating mb-3">
                                <input id="zipcode" type="text" placeholder="Postcode" class="border-success input-fields form-control @error('zipcode') is-invalid @enderror" name="zipcode" value="{{ old('zipcode') }}" required autocomplete="zipcode" autofocus>
                                <label for="zipcode"> Postcode </label>
                                @error('zipcode')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6">
                            <div class="form-floating mb-3">
                                <input id="email" type="email" placeholder="email" class="border-success input-fields form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                <label for="email"> e-mail </label>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating mb-3">
                                <input id="password" type="password" placeholder="Password" class="border-success input-fields form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <label for="password"> Wachtwoord </label>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-floating mb-3">
                                <input id="password-confirm" type="password" placeholder="Confirm Password" class="border-success form-control pb-5 pb-md-3" name="password_confirmation" required autocomplete="new-password">
                                <label for="password-confirm"> Bevestig Wachtwoord</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-5 py-3 dog-field">
                    <div class="row justify-content-start">
                        <div class="col-12">
                            <h3>Hond(en)</h3>
                        </div>
                    </div>
                    <div class="row justify-content-start" id="dog-inputs">
                        {{--DOG INPUT FIELDS GET SET HERE BY JAVASCRIPT--}}
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 dog-input-fields text-center">
                            <div class="form-group ">
                                <button type="button" class="btn dog-add-btn px-4 py-2" onclick="addDog()">+</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-end">
                <div class="col-12 text-end">
                    <button type="button" class="btn btn-success float-end registerBtn m-3 me-1 ps-5 pe-5 pt-1 pb-1 mb-4" onclick="registerUser()">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/registerAddDog.js')}}"></script>
@endsection
