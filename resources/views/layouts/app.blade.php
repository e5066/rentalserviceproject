<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'HondenUitlaatService') }} - @yield('title')</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googlenapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
{{--    <link href="{{ asset('css/webshop.css') }}" rel="stylesheet">--}}
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css"
    />
    <link
        rel="stylesheet"
        href="/css/bootstrap-datetimepicker.min.css"
    />
    <link href="{{ asset('css/sass.css') }}" rel="stylesheet">

</head>
<?php
    $classBody = '';
    switch(Route::current()->uri()){
        case 'login':
            $classBody = 'loginBg';
            break;
        case 'register':
            $classBody = 'registerBg';
            break;
        case 'teams':
            $classBody = 'teamsBg';
            break;
        default:
            $classBody = 'bgStandard';
            break;
    }
?>
<body class="{{$classBody}}">

    <header class="sticky-md-top sticky-lg-top sticky-xl-top sticky-xxl-top">
        @include('layouts.navbar')
    </header>
            @yield('content')
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
    <script src="/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

</body>
</html>
