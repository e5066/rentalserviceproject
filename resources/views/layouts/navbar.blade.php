<head>
    <link rel="stylesheet" href="{{asset('css/navbar.css')}}">
</head>
<div class="container-fluid main-nav">
    <div class="row justify-content-start align-items-start">
        <div class="col-12">
            <nav class="navbar navbar-expand-md navbar navbar-light">
                <div class="container-fluid">
                    <div class="col-12 col-md-1 text-md-start text-center order-md-0 order-0">
                        <a class="navbar-brand m-0" href="#">
                            <img src="{{asset('img/logo3.png')}}" alt="" width="120vh" height="auto" class="d-inline-block align-text-top img-fluid">
                        </a>
                    </div>
                    <div class="col-12 col-md-9 text-center text-md-start pt-3 pt-md-0 ps-md-4 ps-0 order-md-1 order-2">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-start" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link me-0 me-md-5 <?=(Route::current()->uri() == '/' ? 'active' : '')?> " aria-current="page" href="{{ url('/') }}">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link me-0 me-md-5 <?=(Route::current()->uri() == 'shop' ? 'active' : '')?> " href="{{url('shop')}}">Winkel</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link me-0 me-md-5 <?=(Route::current()->uri() == 'blogs' ? 'active' : '')?> " href="{{url('blogs')}}">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link me-0 me-md-5 <?=(Route::current()->uri() == 'overmij' ? 'active' : '')?> " href="{{url('overmij')}}">Over mij</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link me-0 me-md-5 <?=(Route::current()->uri() == 'contact' ? 'active' : '' )?> " href="{{url('contact')}}">Contact</a>
                                </li>
                                @if(\App\Models\Admin::isAdmin())
                                    <li class="nav-item">
                                        <a class="nav-link me-0 me-md-5 <?=(Route::current()->uri() == 'admin' ? 'active' : '' )?> " href="{{url('admin')}}">Admin</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-2 text-center text-md-end pt-md-0 pt-3 order-md-2 order-1">
                        @if(\Illuminate\Support\Facades\Auth::id() == null)
                            <a class="login-btn px-5 py-1 <?=(Route::current()->uri() == 'contact' )?> " href="{{url('login')}}">Login</a>
                        @else
                            <div class="dropdown account-dropdown">
                                <button class="btn btn-success dropdown-toggle account-item" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    {{\Illuminate\Support\Facades\Auth::user()->name}}
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item account-item" href="{{url('profile') . '/' . \Illuminate\Support\Facades\Auth::id()}}">Account</a></li>
                                    <li><a class="dropdown-item account-item" href="{{url('logout')}}">Uitloggen</a></li>
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
