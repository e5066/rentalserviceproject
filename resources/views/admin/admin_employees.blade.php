@extends('admin.admin_board')
@section('title', 'Werknemers')
@section('option')

    <head>
        <script src="{{asset('js/search_customers.js')}}"></script>
    </head>

    <div class="row pb-3">
        <div class="col-12 col-lg-11">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="customerInput" onkeyup="search_customers()" title="Zoek klant via: voornaam, achternaam of e-mail"/>
                <label for="customerInput">Vindt werknemer...</label>
            </div>
        </div>
        <div class="col-12 col-lg-1 align-items-center text-center">
            <a href="{{url('/admin/employees/create')}}" style="color: #51BFA6 !important;" title="Pas rechten aan...">
                <i class="bi bi-pencil-square" style="font-size: 43px !important;"></i>
            </a>
        </div>
    </div>
    <div class="row p-3">
        <div class="card p-0">
            <div class="card-body custom-card">
                <div class="table-responsive">
                    <table class="table table-centered table-nowrap table-hover mb-0" id="customerTable">
                        <tbody>
                        @for($i = 0; $i < count($data['employees']); $i++)
                            <tr>
                                <td>{{$data['employees'][$i]->firstname}}</td>
                                <td>{{$data['employees'][$i]->lastname}}</td>
                                <td>{{$data['employees'][$i]->email}}</td>
                                <td>{{$data['employees'][$i]->roleName}}</td>
                                <td><a class="btn btn-success" href="{{url('admin/employees') . '/' . $data['employees'][$i]->id}}/edit">Bekijken</a></td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
