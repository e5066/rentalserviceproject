@extends('admin.admin_board')
@section('title', 'Klant: ' . $data['user']->name)
@section('option')
    <div class="row card customer-board justify-content-center d-flex px-3 px-lg-4">
        <div class="col-12">
        <div class="row p-3 p-lg-0 justify-content-center justify-content-lg-start text-center text-sm-center text-md-center text-lg-start">
            <img src="data:image;base64,{{base64_encode($data['user']->profile_pic)}}" class="card-img-top img-fluid" alt="profiel foto niet ingesteld" style="width: 12rem; height: auto">
            <h1>{{$data['user']->firstname[0] . '.' . ' ' . $data['user']->lastname}}</h1>
        </div>
        <div class="row justify-content-center justify-content-lg-start">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 pb-3 p-0 pe-lg-3 align-items-lg-stretch">
                <div class="card custom-card">
                    <div class="card-body">
                        <h3>Contactgegevens</h3>
                        <div class="row">
                            <div class="col-12">
                                <label>Voornaam</label>
                                <input class="form-control mb-3" id="disabledInput" type="text" placeholder="{{$data['user']->firstname}}" disabled>
                                <label>Achternaam</label>
                                <input class="form-control mb-3" id="disabledInput" type="text" placeholder="{{$data['user']->lastname}}" disabled>
                                <label>E-mail</label>
                                <input class="form-control mb-3" id="disabledInput" type="text" placeholder="{{$data['user']->email}}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 pb-3 p-0">
                <div class="card custom-card">
                    <div class="card-body">
                        <div class="products">
                            <h3 class="pb-3">Bestelde producten</h3>
                            <div class="row justify-content-evenly">
                                @if(count($data['boughtProducts']) == 0)
                                    <div class="col-12 ms-3 me-3 mb-3 align-items-center align-items-lg-stretch">
                                        <div class="row pt-3 pb-0 text-center">
                                            <h4 class="link-warning">Geen producten aangeschaft</h4>
                                        </div>
                                    </div>
                                @else
                                    @foreach($data['boughtProducts'] as $product)
                                        <div class="col-10 col-sm-10 col-md-10 col-lg-5 col-xl-5 col-xxl-5 card ms-3 me-3 mb-3 custom-card align-items-center align-items-lg-stretch">
                                            <div class="row pt-3 pb-0">
                                                <h4>{{$product->name}} x {{$product->productCount}}</h4>
                                            </div>
                                            <div class="row pt-1 pb-1">
                                                <p>{{$product->description}}</p>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <span class="badge bg-success rounded-pill">Gebruikt: {{$product->used}}</span>
                                                </div>
                                            </div>
                                            <div class="row pb-3">
                                                <div class="col-4">
                                                    <span class="badge bg-success rounded-pill">Over: {{$product->total - $product->used}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 p-0 pb-3">
                <div class="card custom-card">
                    <div class="card-body">
                        <div class="products">
                            <h3>Hond(en)</h3>
                            <div class="row justify-content-center justify-content-lg-start align-items-start p-3">
                                @if(count($data['customerDogs']) == 0)
                                    <div class="col-12  ms-3 me-3 mb-3 align-items-center align-items-lg-stretch">
                                        <div class="row pt-3 pb-0 text-center">
                                            <h4 class="link-warning">Geen honden toegevoegd</h4>
                                        </div>
                                    </div>
                                @else
                                    @foreach($data['customerDogs'] as $dog)
                                        <div class="col-12 card custom-card p-0 mb-3 me-md-5" style="width: 18rem;">
                                            <img src="data:image;base64,{{base64_encode($dog->dog_pic)}}" class="card-img-top img-fluid" alt="Foto van de hond">
                                            <div class="card-body">
                                                <h5 class="card-title">{{$dog->name}}</h5>
                                                <div class="accordion" id="accordionDetails">
                                                    <div class="accordion-item">
                                                        <h2 class="accordion-header" id="headingOne">
                                                            <button class="accordion-button detail-btn" type="button" data-bs-toggle="collapse" data-bs-target="#{{$dog->name}}Details" aria-expanded="true" aria-controls="{{$dog->name}}Details">
                                                                Details
                                                            </button>
                                                        </h2>
                                                        <div id="{{$dog->name}}Details" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionDetails">
                                                            <div class="accordion-body p-0 pt-1">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        <h6>Leeftijd</h6>
                                                                        <span class="badge bg-success rounded-pill">{{$dog->age}}</span>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        <div class="row justify-content-start">
                                                                            <div class="col-12">
                                                                                <h6>Allergiën</h6>
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <span class="badge bg-success rounded-pill text-wrap">{{$dog->allergies}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                        <div class="row justify-content-start">
                                                                            <div class="col-12">
                                                                                <h6>Weetjes</h6>
                                                                            </div>
                                                                            <div class="col-12">
                                                                                <p>{{$dog->description}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
