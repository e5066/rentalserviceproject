@extends('admin.admin_board')

@section('mainDashboard')
    <div class="container-fluid">
        <div class="row">
            <div class="col-auto">
                <h1>Dashboard</h1>
            </div>
        </div>
        <div class="row justify-content-center justify-content-lg-start">
            <div class="col-12 col-lg-4 custom-card h-auto align-self-start me-lg-3 mb-3">
                <h5 class="p-3">Verkochte producten</h5>
                <div id="productBoughtDonut" class="p-3"></div>
            </div>
            <div class="col-12 col-lg-4 custom-card h-auto align-self-start me-lg-3 mb-3">
                <h5 class="p-3">Omzet</h5>
                <div id="profitDonut" class="p-3"></div>
            </div>
            <div class="col-12 col-lg-7 custom-card h-auto align-self-start mb-3">
                <h5 class="p-3">Aantal per categorie verkocht in {{date("Y")}}</h5>
                <div id="salesMonthlySmooth" class="p-3" style="min-height: 50vh"></div>
            </div>
        </div>
    </div>
    <script lang="js">
    {{--    Generate a pie/donut chart of bought products    --}}
    @if(!empty($data['productsBought'][0]))
        var options = {
            chart: {
                type: 'donut'
            },
            series: [{{$data['productsBought'][0]->total}}, {{$data['productsBought'][1]->total}}, {{$data['productsBought'][2]->total}}],
            labels: ['10 Strippenkaart', '5 Strippenkaart', 'Enkele wandeling']
        }

        var chart = new ApexCharts(document.querySelector("#productBoughtDonut"), options);
        chart.render();

    {{--    Generate a smooth area chart of sales per day    --}}
        var options2 = {
            series: [{
                name: 'Aantal verkocht',
                data: [
                    @for($i = 0; $i < count($data['productsBought'][3]); $i++)
                        {{$data['productsBought'][3][$i]->total}},
                    @endfor
                ]
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    borderRadius: 4,
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: [
                    @for($i = 0; $i < count($data['productsBought'][3]); $i++)
                        '{{$data['productsBought'][3][$i]->name}}',
                    @endfor
                ],
            }
        }
        var chart2 = new ApexCharts(document.querySelector("#salesMonthlySmooth"), options2);
        chart2.render();

    {{--    Generate a pie/donut chart of profits    --}}
    var options = {
        chart: {
            type: 'donut',
        },
        series: [
            @for($i = 0; $i < count($data['productsBought'][4]); $i++)
                {{$data['productsBought'][4][$i]->total * $data['productsBought'][4][$i]->price}},
            @endfor
        ],
        labels: [
            @for($i = 0; $i < count($data['productsBought'][4]); $i++)
                '{{$data['productsBought'][4][$i]->name . ' €'}}',
            @endfor
        ]
    }

    var chart3 = new ApexCharts(document.querySelector("#profitDonut"), options);
    chart3.render();
    @endif
    </script>
@endsection
