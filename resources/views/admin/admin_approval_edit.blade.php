@extends('admin.admin_board')

@section('option')

<div class="container">
    <div class="row">
        <div class="card p-0">
            <div class="card-header custom-card text-center">
                <h6>Walks Aanpassen</h6>
            </div>
            <div class="card-body custom-card justify-content-center text-center">
                <form method="POST" action="{{route('approval.update', $approvedWalks[0]->walk_id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group ">
                                <input type="hidden" name="id" value="{{$approvedWalks[0]->walk_id}}">
                            </div>
                            <div class="form-group mt-3 mb-2">
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <h6>Walks:</h6>
                                    </div>
                                </div>
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <input type="text" name="approve" value="{{$approvedWalks[0]->approve}}"><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-5 mb-2">
                                <input type="submit" value="Submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
