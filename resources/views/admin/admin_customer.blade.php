@extends('admin.admin_board')

@section('option')

    <head>
        <script src="{{asset('js/search_customers.js')}}"></script>
    </head>

    <div class="row pb-3">
        <div class="col-12">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="customerInput" onkeyup="search_customers()" title="Zoek klant via: voornaam, achternaam of e-mail"/>
                <label for="customerInput">Vindt klant...</label>
            </div>
            <div class="card">
                <div class="card-body custom-card">
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap table-hover mb-0" id="customerTable">
                            <tbody>
                            @for($i = 0; $i < count($data['customers']); $i++)
                                <tr>
                                    <td>{{$data['customers'][$i]->firstname}}</td>
                                    <td>{{$data['customers'][$i]->lastname}}</td>
                                    <td>{{$data['customers'][$i]->email}}</td>
                                    <td><a class="btn btn-success" href="{{url('admin/customers') . '/' . $data['customers'][$i]->id}}">Bekijken</a></td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
