@extends('admin.admin_board')

@section('option')

    <div class="container">
        <div class="row">
            <div class="card p-0">
                <div class="card-header custom-card text-center">
                    <h6>Product Aanpassen</h6>
                </div>
                <div class="card-body custom-card justify-content-center text-center">
                        <form method="POST" action="{{route('product.update', $product->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group ">
                                    <input type="hidden" name="id" value="{{$product->id}}">
                                        <div class="row justify-content-center text-center">
                                            <div class="col-12 col-lg-5">
                                                <h6>Name:</h6>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center text-center">
                                            <div class="col-12 col-lg-5">
                                                <input type="text" name="name" value="{{$product->name}}"><br>
                                            </div>
                                            <div class="col-12">
                                                @error('name')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3 mb-2">
                                        <div class="row justify-content-center text-center">
                                            <div class="col-12 col-lg-5">
                                                <h6>Prijs:</h6>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center text-center">
                                            <div class="col-12 col-lg-5">
                                                <input type="text" name="price" value="{{$product->price}}"><br>
                                            </div>
                                            <div class="col-12">
                                                @error('price')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3 justify-content-center text-center ">
                                        <div class="col-12 col-lg-5">
                                            <h6>Walkcount:</h6>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center text-center">
                                        <div class="col-12 col-lg-5">
                                            <input type="text" name="walkcount" value="{{$product->walkcount}}"><br>
                                        </div>
                                        <div class="col-12">
                                            @error('walkcount')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-5 justify-content-center text-center">
                                        <div class="col-12 col-lg-5">
                                            <h6>Oude afbeelding</h6>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center text-center">
                                        <div class="col-12 col-lg-5">
                                            <img src="{{asset('uploads/product/'.$product->img)}}" width="70" height="70px" alt="">
                                        </div>
                                    </div>
                                    <div class="row mt-4 justify-content-center text-center">
                                        <div class="col-12 col-lg-5 mb-2">
                                            <h6>Beschrijving:</h6>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center text-center">
                                        <div class="col-12 col-lg-5">
                                            <textarea input type="text" class="form-control"  name="description" rows="5" value="{{$product->description}}"></textarea>
                                        </div>
                                        <div class="col-12">
                                            @error('description')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3 justify-content-center text-center">
                                        <div class="col-12 col-lg-5">
                                            <h6>Upload nieuwe Afbeelding:</h6>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center text-center">
                                        <div class="col-12 col-lg-5">
                                            <input type="file" name="image" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group mt-5 mb-2">
                                        <input type="submit" value="Submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection
