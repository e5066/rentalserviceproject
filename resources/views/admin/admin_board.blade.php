@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<head>
    <link href="{{asset('css/admin_board.css')}}" rel="stylesheet">
</head>
@if(!empty(Route::current()->parameters['customer']))
    <?php
        $active = 'active';
    ?>
@else
    <?php
    $active = '';
    ?>
@endif
<div class="container-fluid content">
    <div class="row align-items-stretch admin-board-bg d-flex">
        <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 col-xxl-2 ps-0 pe-0 vertical-navbar position-sticky">
            <div id="vnav" class="list-group vertical-navbar">
                <a href="{{url('admin')}}" class="list-group-item list-group-item-action {{(Route::current()->uri() == 'admin') ? 'active' : ''}}" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Dashboard</h5>
                    </div>
                    <p class="mb-1">Overzicht: sales</p>
                </a>
                <a href="{{url('admin/approval')}}" class="list-group-item list-group-item-action {{(Route::current()->uri() == 'admin/approval') ? 'active' : ''}} {{($active == 'active') ? 'active' : ''}}">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Aangevraagde wandelingen</h5>
                    </div>
                    <p class="mb-1">Agenda: klanten die hun hond willen laten uitlaten en wachten op akkoord.</p>
                </a>
                <a href="{{url('admin/recap')}}" class="list-group-item list-group-item-action {{(Route::current()->uri() == 'admin/recap') ? 'active' : ''}} {{($active == 'active') ? 'active' : ''}}">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Terugblik wandelingen</h5>
                    </div>
                    <p class="mb-1">Terugkoppeling: mogelijkheid om een recap te schrijven van de wandeling.</p>
                </a>
                <a href="{{url('admin/product')}}" class="list-group-item list-group-item-action {{(Route::current()->uri() == 'admin/product') ? 'active' : ''}} {{($active == 'active') ? 'active' : ''}}">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Producten</h5>
                    </div>
                    <p class="mb-1">Producten overzicht: Voeg hier nieuwe producten toe of pas bestaande producten aan.</p>
                </a>
                <a href="{{url('admin/customers')}}" class="list-group-item list-group-item-action {{(Route::current()->uri() == 'admin/customers') ? 'active' : ''}} {{($active == 'active') ? 'active' : ''}}">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Klanten</h5>
                    </div>
                    <p class="mb-1">Inzage: Bekijk klantgegevens.</p>
                </a>
                <a href="{{url('admin/employees')}}" class="list-group-item list-group-item-action {{(Route::current()->uri() == 'admin/employees') ? 'active' : ''}} {{(Route::current()->uri() == 'admin/employees/create') ? 'active' : ''}}">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Werknemers</h5>
                    </div>
                    <p class="mb-1">Werknemers: Voeg hier nieuwe medewerkers toe, pas medewerkers aan of verwijder werknemers.</p>
                </a>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 align-self-start p-0 pt-4">
            <div class="container-fluid">
                @if(Route::current()->uri() == 'admin')
                    @yield('mainDashboard')
                @else
                    @yield('option')
                @endif
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/admin_board.js')}}"/>
@endsection
