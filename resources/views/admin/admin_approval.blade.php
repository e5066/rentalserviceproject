@extends('admin.admin_board')

@section('option')

    <head>
        <script src="{{asset('js/search_walks.js')}}"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <div class="row">
        <div class="col-12">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="walksInput" onkeyup="search_walks()" title="Zoek klant via: voornaam, achternaam of e-mail"/>
                <label for="walksInput">Zoek walks...</label>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-success" id="walksTable">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Status</th>
                        <th scope="col">Hond Naam</th>
                        <th scope="col">Tijd</th>
                        <th scope="col">Datum</th>
                        <th scope="col">Eigenaar Naam</th>
                        <th scope="col">Verzend Goedkeuring</th>
                        <th scope="col">Verzend Annulering</th>
                    </tr>
                    </thead>
                    <tbody>
                    @for($i = 0; $i < count($data['approvedWalks']); $i++)
                        <tr>
                            <th scope="row">{{$i + 1}}</th>
                            <td id="{{$data['approvedWalks'][$i]->dog_id}}" class="approved-text">Status ophalen...</td>
                            <td>{{$data['approvedWalks'][$i]->name}}</td>
                            <td>{{date_create($data['approvedWalks'][$i]->time)->format('H:i')}}</td>
                            <td>{{date_format(date_create($data['approvedWalks'][$i]->date), 'd/M/Y')}}</td>
                            <td>{{$data['approvedWalks'][$i]->firstname}}</td>



                            <td>
                                <button type="button" class="btn-success" onclick="return confirm('Wilt u de huidige afspraak Goedkeuren?'), acceptWalk({{$data['approvedWalks'][$i]->id}}, {{$data['approvedWalks'][$i]->dog_id}})">
                                    <i class="bi bi-pencil" style="font-size: 26px !important;"></i>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn-danger" onclick="return confirm('Wilt u de huidige afspraak annuleren? (Stuurt automatisch een email naar de klant)'), denyWalk('{{$data['approvedWalks'][$i]->firstname}}', '{{$data['approvedWalks'][$i]->name}}', '{{$data['approvedWalks'][$i]->time}}', '{{$data['approvedWalks'][$i]->date}}', '{{$data['approvedWalks'][$i]->email}}', '{{$data['approvedWalks'][$i]->dog_id}}', '{{$data['approvedWalks'][$i]->id}}')">
                                    <i class="bi bi-x-circle-fill" style="font-size: 26px !important;"></i>
                                </button>
                            </td>
                        </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function acceptWalk(id, dog){
            sendWalkAccept(id, dog);
        }

        function denyWalk(firstname, dogName, timeslot, walkDate, email, dogId, walkId){
            sendWalkDeny(firstname, dogName, timeslot, walkDate, email, dogId, walkId);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function sendWalkAccept(id, dog) {
            $.ajax({
                url: '/admin/approval/' + id + "/" + dog,
                type: 'put',
                datatype: 'json',
                data: {'id' : id, 'dog' : dog},
                beforeSend: function() {
                    document.getElementById(dog).innerHTML = "Verzenden...";
                }
            })
                .done(function(data) {
                    document.getElementById(dog).innerHTML = data.response;
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('COULD NOT COMPLETE AJAX REQUEST');
                });
        }

        function sendWalkDeny(firstname, dogName, timeslot, walkDate, email, dogId, walkId){
            const walk = [firstname, dogName, timeslot, walkDate, email, dogId, walkId];
            $.ajax({
                url: '/admin/approval/deny/' + walk,
                type: 'get',
                datatype: 'json',
                data: {'walk' : walk},
                beforeSend: function() {
                    document.getElementById(walk[5]).innerHTML = "Bezig met Annuleren...";
                }
            })
                .done(function(data) {
                    document.getElementById(walk[5]).innerHTML = data.response;
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log('COULD NOT COMPLETE AJAX REQUEST');
                });
        }
        function onload(){
            $.ajax({
                url: '/admin/approval/walks',
                type: 'get',
                datatype: 'json',
                beforeSend: function() {
                }
            })
                .done(function(data) {
                    data.list.forEach(walk => document.getElementById(walk.dog_id).innerHTML = returnApprove(walk.approve, walk.status));
                })
        }

        function returnApprove(walk_approve, walk_status){
            if (walk_approve === 1 && walk_status === 1){
                return 'Goedgekeurd';
            }
            else if (walk_approve === 0 && walk_status === 0){
                return 'In behandeling...';
            }
            else if (walk_approve === 0 && walk_status === 2){
                return "Geannuleerd";
            }
            else{
                return "Status onbekend...";
            }

        }

        window.onload = onload();
    </script>
@endsection
