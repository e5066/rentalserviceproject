@extends('admin.admin_board')

@section('option')

    <div class="container">
        <div class="row">
            <div class="card p-0">
                <div class="card-header custom-card text-center">
                    <h4>Product Toevoegen</h4>
                </div>
                    <div class="card-body custom-card justify-content-center text-center">
                        <form method="POST" enctype="multipart/form-data" action="{{route('product.store')}}">
                        @csrf
                        @method('POST')
                        <div class="row">
                            <div class="col-12 mt-3">
                                <div class="row mt-4 justify-content-center text-center ">
                                    <div class="col-12 col-lg-5">
                                        <h6>Productnaam:</h6>
                                    </div>
                                </div>
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <input type="text" name="name" >
                                    </div>
                                    <div class="col-12">
                                        @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mt-3 justify-content-center text-center ">
                                    <div class="col-12 col-lg-5">
                                        <h6>Prijs:</h6>
                                    </div>
                                </div>
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <input type="text" name="price" >
                                    </div>
                                    <div class="col-12">
                                        @error('price')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mt-3 justify-content-center text-center ">
                                    <div class="col-12 col-lg-5">
                                        <h6>Walkcount:</h6>
                                    </div>
                                </div>
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <input type="text" name="walkcount" >
                                    </div>
                                    <div class="col-12">
                                        @error('walkcount')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mt-4 justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <h6>Afbeelding:</h6>
                                    </div>
                                </div>
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <input type="file" name="image">
                                    </div>
                                </div>
                                <div class="row mt-4 justify-content-center text-center ">
                                    <div class="col-12 col-lg-5">
                                        <h6>Beschrijving:</h6>
                                    </div>
                                </div>
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-lg-5">
                                        <textarea input type="text" class="form-control"  name="description" rows="10" ></textarea>
                                    </div>
                                    <div class="col-12">
                                        @error('description')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mt-2 mb-2">
                                <input type="submit" value="Submit">
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>

@endsection
