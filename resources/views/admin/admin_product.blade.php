@extends('admin.admin_board')

@section('option')


    <div class="row">
        <div class="col-12">
            <div class="row justify-content-end py-3">
                <div class="col-3 float-end justify-content-end text-end">
                    <a href="{{url('admin/product/create')}}" class="btn btn-primary" type="button">Product Toevoegen</a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-success" id="customerTable">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product</th>
                        <th scope="col">Prijs</th>
                        <th scope="col">Walkcount</th>
                        <th scope="col">Description</th>
                        <th scope="col">Img</th>
                        <th scope="col">Aanpassingen</th>
                        <th scope="col">Verwijderen</th>
                    </tr>
                    </thead>
                    <tbody>
                    @for($i = 0; $i < count($data['products']); $i++)
                        <tr>
                            <th scope="row">{{$i + 1}}</th>
                            <td>{{$data['products'][$i]->name}}</td>
                            <td>{{$data['products'][$i]->price}}</td>
                            <td>{{$data['products'][$i]->walkcount}}</td>
                            <td>{{$data['products'][$i]->description}}</td>
                            <td>
                                <img src="{{asset('uploads/product/'.$data['products'][$i]->img)}}" width="70" height="70px" alt="">
                            </td>
                            <td>
                                <a class="btn btn-success" href="{{url('admin/product') . '/' . $data['products'][$i]->id}}/edit">
                                    <i class="bi bi-pencil" style="font-size: 26px !important;"></i>
                                </a>
                            </td>
                            <td>
                                <form method="post" action="{{route('product.delete', $data['products'][$i]->id)}}" onsubmit="return confirm('Wilt u het huidige geselecteerde item verwijderen?')">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="delete-btn btn-danger">
                                        <i class="bi bi-trash" style="font-size: 26px !important;"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
