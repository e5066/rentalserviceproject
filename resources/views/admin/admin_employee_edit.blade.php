@extends('admin.admin_board')
@section('title', 'Medewerker: ' . $data['employee'][0]->name)
@section('option')
    <div class="row card customer-board justify-content-center d-flex px-3 px-lg-4">
        <div class="col-12">
            <div class="row p-3 p-lg-0 justify-content-center justify-content-lg-start text-center text-sm-center text-md-center text-lg-start">
                <img src="data:image;base64,{{base64_encode($data['employee'][0]->profile_pic)}}" class="card-img-top img-fluid" alt="profiel foto niet ingesteld" style="width: 12rem; height: auto">
                <h1 class="p-lg-0 p-3">{{$data['employee'][0]->firstname[0] . '.' . ' ' . $data['employee'][0]->lastname}}</h1>
            </div>
            <div class="row justify-content-center justify-content-lg-start">
                <div class="col-12 pb-3 p-0 pe-lg-3 align-items-lg-stretch text-start">
                    <div class="card custom-card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-10">
                                    <h3>Gegevens</h3>
                                </div>
                                <div class="col-2">
                                    <form method="POST" action="{{route('admin.employee.destroy', $data['employee'][0]->id)}}">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn float-end text-end" title="Verwijder account..." type="submit"><i class="bi bi-trash float-end text-end" style="font-size: 28px !important; color: red !important;"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <form method="POST" action="{{route('admin.employee.update', $data['employee'][0]->id)}}">
                                        @method('PUT')
                                        @csrf
                                        <label>Voornaam</label>
                                        <input class="form-control mb-3" id="disabledInput" type="text" placeholder="{{$data['employee'][0]->firstname}}" disabled>
                                        <label>Achternaam</label>
                                        <input class="form-control mb-3" id="disabledInput" type="text" placeholder="{{$data['employee'][0]->lastname}}" disabled>
                                        <label>E-mail</label>
                                        <input name="email" class="form-control mb-3" id="disabledInput" type="text" placeholder="{{$data['employee'][0]->email}}">
                                        <div class="form-floating mb-3">
                                            <select name="employeeJob" class="form-select" id="floatingSelectFunction" aria-label="Selecteer functie">
                                                @foreach($data['jobList'] as $job)
                                                    <option value="{{$job->id}}">{{$job->name}}</option>
                                                @endforeach
                                                <option selected value="{{$data['employee'][1][0]->id}}" disabled>{{$data['employee'][1][0]->name}}</option>
                                            </select>
                                            <label for="floatingSelectFunction">Selecteer functie</label>
                                        </div>
                                        <input type="submit" class="btn custom-submit-btn2" value="Aanpassen">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
