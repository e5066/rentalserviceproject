@extends('admin.admin_board')
@section('title', 'Werknemer toevoegen')
@section('option')
    <div class="row justify-content-center">
        <div class="col-12 align-items-center text-center">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('success')}}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <form method="POST" action="{{route('admin.employee.store', )}}" class="justify-content-center">
            @method('PUT')
            @csrf
                <div class="form-floating mb-3">
                    <select name="user" class="form-select" id="floatingSelectEmployee" aria-label="Selecteer medewerker">
                        @foreach($data['everybody'] as $person)
                            <option value="{{$person->id}}">{{$person->name}}</option>
                        @endforeach
                    </select>
                    <label for="floatingSelectFunction">Wie wordt de nieuwe medewerker?</label>
                </div>
                <div class="form-floating mb-3">
                    <select name="employeeJob" class="form-select" id="floatingSelectFunction" aria-label="Selecteer functie">
                        @foreach($data['jobList'] as $job)
                            <option value="{{$job->id}}">{{$job->name}}</option>
                        @endforeach
                    </select>
                    <label for="floatingSelectFunction">Selecteer functie</label>
                </div>
                <input class="custom-submit-btn" name="addNewEmployee" type="submit" value="Updaten">
            </form>
        </div>
    </div>
    <div class="row justify-content-center py-3">
        <div class="card p-0">
            <div class="card-body custom-card">
                <div class="table-responsive">
                    <table class="table table-centered table-nowrap table-hover mb-0" id="customerTable">
                        <tbody>
                        @for($i = 0; $i < count($data['employees']); $i++)
                            <tr>
                                <td>{{$data['employees'][$i]->firstname}}</td>
                                <td>{{$data['employees'][$i]->lastname}}</td>
                                <td>{{$data['employees'][$i]->email}}</td>
                                <td>{{$data['employees'][$i]->roleName}}</td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
