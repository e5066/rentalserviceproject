@extends('admin.admin_board')

@section('option')
    <div class="container">
        <div class="row">
            <div class="card p-0">
                <div class="card-header custom-card text-center">
                    <h4>Terugkoppeling van de wandeling.</h4>
                </div>
                    <div class="card-body custom-card justify-content-center text-center">
                        @if(Session::has('recap_sent'))
                            <div class="alert alert-success" id="alert">
                                <div class="row">
                                    <div class="col-md-10">
                                        {{Session::get('recap_sent')}}
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="close justify-content-end" data-bs-dismiss="alert"><i class="contact-close-btn bi-x"></i></button>
                                    </div>
                                </div>
                            </div>
                        @endif

                    <form method="POST" action="{{route('recap.send')}}" enctype="multipart/form-data">
                        @csrf

                        @method('POST')
                        <div class="row mt-5 mb-5">
                            <div class="col-6">
                                <div class="form-group mt-2">
                                    <div id="options">
                                            <label for="msg"><h6>Gewandelde honden: </h6></label>
                                            <select name="selectedDog" id="dogSelect">
                                                @foreach($data['dogs'] as $dog)
                                                    <option value="{{json_encode($dog)}}">{{$dog->name}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group ">
                                    <label for="msg"><h6>Extra hond toevoegen aan een recap: </h6></label>
                                    <button type="button" class="btn" onclick="addOption()">+</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="msg"><h6>Bericht/Recap verzenden naar de klant:</h6></label>
                            <textarea input type="text" class="form-control"  name="msg" rows="5" ></textarea>
                            @error('msg')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-5 mb-5">
                        <input type="submit" class= "ryan ryan float-right" value="Verzenden">
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <script lang="js">
        let i = 0;
        function addOption(){
            let oldOption = document.getElementById('dogSelect');
            console.log(oldOption);
            let cloneOption = oldOption.cloneNode(true);
            document.getElementById('options').appendChild(cloneOption);
            document.getElementById(cloneOption.id).name = document.getElementById(cloneOption.id).name + i;
            document.getElementById(cloneOption.id).id = document.getElementById(cloneOption.id).id + i;
            i++;
        }
    </script>

@endsection
