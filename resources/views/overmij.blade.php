@extends('layouts.app')

@section('content')
    <div class="checkout">
        <div class="checkout-form">
            <div style="float:left; width: 32%">
                <img src="{{asset('img/jamy-with-dogs.jpg')}}" alt="niet geonvden" style="widtH:100%">
            </div>
             <div style="float:right; width: 63%">
                 <h3>Wie ben ik?</h3>
                 <a>Mijn naam is Jamy. Ik studeer HBO Pedagogiek in Zwolle. Deze studie heb ik bijna afgerond. Ik heb mijn
                     hele leven huisdieren gehad, dieren om me heen gehad en verzorgd, waaronder ook honden. Momenteel pas ik
                     regelmatig op huisdieren van vrienden en kennissen en heb ik een kat, Kaya en een hond, een Västgötaspets
                     genaamd Cajsa.
                 </a>
                 <h3 style="margin-top:20px">Mijn liefde voor honden</h3>
                 <a>Ik ben zeer geïnteresseerd in dieren en dan vooral honden. Als vanaf jongs af aan las het ene na het
                     andere boek over hondenverzorging en –rassen en paste ik op honden van vrienden. Zodra de mogelijkheid
                     er was, heb ik mijn eerste eigen hond gekocht. Ik begon honden en hun gedrag zo leuk te vinden dat ik
                     graag met honden wilde werken. Hierdoor is het idee ontstaan om zelf een uitlaatservice te beginnen.
                     In oktober 2019 heb ik hiervoor de opleiding Gediplomeerd uitlaatservice gevolgd bij Martin Gaus.
                 </a>
             </div>
        </div>
    </div>

@endsection
