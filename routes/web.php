<?php

use App\Http\Controllers\DogsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CustomerAdminController;
use App\Http\Controllers\EmployeesAdminController;
use App\Http\Controllers\WalksController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductAdminController;
use App\Http\Controllers\RecapAdminController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ApprovalAdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\BlogsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
//START All admin related routes
Route::get('/admin', [AdminController::class, 'index']);//Yeray
Route::get('/admin/customers', [CustomerAdminController::class, 'index']);//Yeray
Route::get('/admin/customers/{customer}', [CustomerAdminController::class, 'show']);//Yeray
Route::get('/admin/employees', [EmployeesAdminController::class, 'index']);//Yeray
Route::get('/admin/employees/create', [EmployeesAdminController::class, 'create']);//Yeray
Route::put('/admin/employees/{employee}', [EmployeesAdminController::class, 'update'])->name('admin.employee.update');//Yeray
Route::delete('/admin/employees/{employee}', [EmployeesAdminController::class, 'destroy'])->name('admin.employee.destroy');//Yeray
Route::put('/admin/employees', [EmployeesAdminController::class, 'store'])->name('admin.employee.store');//Yeray
Route::get('/admin/employees/{employee}/edit', [EmployeesAdminController::class, 'edit']);//Yeray

Route::get('/admin/product/create', [ProductAdminController::class, 'create'])->name('product.create');//ryan
Route::post('/store', [ProductAdminController::class, 'store'])->name('product.store');//ryan
Route::get('/admin/product', [ProductAdminController::class, 'index'])->name('product.look'); //ryan
Route::get('/admin/product/{id}/edit', [ProductAdminController::class, 'edit']);//ryan
Route::put('/admin/product/{id}', [ProductAdminController::class, 'update'])->name('product.update');//ryan
Route::get('/admin/product/{id}', [ProductAdminController::class, 'show'])->name('product.show');//ryan
Route::get('/admin/recap', [RecapAdminController::class, 'index']);//ryan
Route::post('/send-recap', [RecapAdminController::class, 'sendEmail'])->name('recap.send');//ryan sent recap to mail
Route::get('/contact', [ContactController::class, 'index']);//ryan contactpage
Route::post('/send-message', [ContactController::class, 'sendEmail'])->name('contact.send');//ryan contactpage
Route::DELETE('/admin/product/delete/{id}', [ProductAdminController::class, 'destroy'])->name('product.delete');//ryan

Route::get('/admin/approval', [ApprovalAdminController::class, 'index'])->name('approval.look');//ryan
Route::get('/admin/approval/{id}/edit', [ApprovalAdminController::class, 'edit']);//ryan
Route::put('/admin/approval/{id}/{dog}', [ApprovalAdminController::class, 'update'])->name('approval.update');
Route::get('/admin/approval/deny/{walk}', [ApprovalAdminController::class, 'destroy'])->name('approval.destroy');
Route::get('/admin/approval/walks', [ApprovalAdminController::class, 'show']);
//END

Route::get('walk', [WalksController::class, 'index']);
Route::post('create-walk', [WalksController::class, 'create']);
Route::get('user/dogs', [DogsController::class, 'dogsFromUser']);
Route::get('timeslots', [WalksController::class, 'getTimeSlots']);
Route::get('walks-per-month', [WalksController::class, 'getWalksPerMonth']);
Route::get('walks-available', [WalksController::class, 'getWalksAvailablePerUser']);

Route::get('shop', [ProductsController::class, 'index'])->name('shop');
Route::get('pending', [ProductsController::class, 'returnPendingView'])->name('pending');
Route::get('checkout/{product}', [ProductsController::class, 'checkout']);
Route::post('checkout/{product}', [ProductsController::class, 'finalizeCheckout']);
Route::name('webhooks.mollie')->post('webhooks/mollie', 'ProductsController@handle');
Route::get('check-status/{id}', [ProductsController::class, 'checkStatus'])->name('check-status');

Auth::routes();

//Profile
Route::get('profile/{profile}', [UserController::class, 'show']);
Route::get('profile/{profile}/edit', [UserController::class, 'edit']);
Route::patch('profile/{profile}', [UserController::class, 'update']);
//Profile Password
Route::get('profile/{profile}/editpassword', [UserController::class, 'editpassword']);
Route::patch('profile/{profile}/password', [UserController::class, 'updatepassword']);
//Profile Dogs
Route::get('profile/{profile}/honden', [UserController::class, 'showdogs']);
Route::get('profile/{profile}/{hond}/edit', [UserController::class, 'editdog']);
Route::patch('profile/{profile}/{hond}', [UserController::class, 'updatedog']);
Route::get('profile/{profile}/honden/create', [UserController::class, 'createdog']);
Route::post('/profile/{profile}/honden', [UserController::class, 'storedog']);
Route::delete('profile/{profile}/{hond}', [UserController::class, 'destroydog']);
//Profile Strippenkaarten
Route::get('profile/{profile}/kaarten', [UserController::class, 'showkaarten']);

Route::get('dog-show',[DogsController::class,'show']);
Route::get('dogs',[DogsController::class,'index'])->name('dogs.index')->name('dogs.index');//show all records
Route::get('dogs/create',[DogsController::class,'create'])->name('dogs.create')->name('dog.create');//create new record
Route::post('dogs',[DogsController::class,'store'])->name('dogs.store')->name('dog.store');//save record
Route::get('dogs/{dog}',[DogsController::class,'show'])->name('dogs.show')->name('dog.show');//select one record
Route::get('dogs/{dog}/edit',[DogsController::class,'edit'])->name('dogs.edit')->name('dog.edit');//edit one record
Route::patch('dogs/{dog}',[DogsController::class,'update'])->name('dogs.update')->name('dog.update');//update one record
Route::delete('dogs/{dog}',[DogsController::class,'destroy'])->name('dogs.delete')->name('dog.delete');//delete one record

//START All blog related routes
Route::get('/blogs', [BlogsController::class, 'index']);
Route::get('/blog/{page}', [BlogsController::class, 'allBlogs']);
Route::put('/blog/update/{id}', [BlogsController::class, 'update']);
Route::delete('/blog/delete/{id}', [BlogsController::class, 'destroy']);
//END

Route::get('overmij', function () {
    return view('overmij');
});

Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
