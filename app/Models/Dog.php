<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dog extends Model
{
    use HasFactory;

    protected $fillable = [
        'permissionPhotos','name','age','allergies','description','user_id', 'dog_pic'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function walks()
    {
        return $this->belongsToMany(
            Walk::class,
            'dog_walk',
            'walk_id',
            'dog_id');
    }
}
