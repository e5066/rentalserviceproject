<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Support\Jsonable;

class Blog extends Model implements Jsonable
{
    use HasFactory;

    protected $fillable = [
        'text',
        'photo',
        'likes'
    ];

    public function dogs()
    {
        return $this->belongsToMany(
            Dog::class,
            'blog_dog',
            'dog_id',
            'blog_id');
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'blog_user',
            'user_id',
            'blog_id');
    }

    public static function getAllBlogs($index)
    {
        return Blog::selectRaw('*, date_format(convert(created_at, DATE), "%d-%m-%Y") as postDate')->orderBy('created_at')->offset($index)->limit(6)->get();
    }

    public static function checkLike($blog_id)
    {
        //count all likes of a blog SQL EXAMPLE = Select COUNT(blog_user.liked) as likes from blog_user inner join blogs on blog_user.blog_id = blogs.id where blogs.id = 14 AND blog_user.liked = 1;
        $likes = DB::table('blog_user')
                ->select('blog_user.liked')
                ->where([['blog_user.blog_id', '=', $blog_id], ['blog_user.user_id', '=', Auth::id()]])
                ->get();
        if (count($likes) == 0){
            return 2;
        }
        else{
            if ($likes[0]->liked == 0){
                return 0;
            }
            else{
                return $likes[0]->liked;
            }
        }
    }

    public static function countLikes($blog_id)
    {
        return DB::table('blog_user')
            ->selectRaw("COUNT(blog_user.liked) as likesCount")
            ->join('blogs', 'blog_user.blog_id', '=', 'blogs.id')
            ->where([
                ['blogs.id', '=', $blog_id],
                ['blog_user.liked', '=', 1]
            ])->get();
    }

    public static function addLike($blog_id)
    {
        $isLiked = self::checkLike($blog_id);
        if ($isLiked == 2){
            DB::table('blog_user')
                ->insert([
                    'user_id' => Auth::id(),
                    'blog_id' => $blog_id,
                    'liked' => 1
                ]);
        }
        else{
            DB::table('blog_user')
                ->where([['blog_user.blog_id', '=', $blog_id], ['blog_user.user_id', '=', Auth::id()]])
                ->update(['blog_user.liked' => 1]);
        }
    }

    public static function removeLike($blog_id){
        DB::table('blog_user')
            ->where([['blog_user.blog_id', '=', $blog_id], ['blog_user.user_id', '=', Auth::id()]])
            ->update(['blog_user.liked' => 0]);
    }
}
