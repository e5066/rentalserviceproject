<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'firstname',
        'lastname',
        'email',
        'password',
        'role_id',
        'profile_pic',
        'street',
        'home_no',
        'zipcode',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'product_user',
            'product_id',
            'user_id');
    }

    public function likes()
    {
        return $this->belongsToMany(
            Blog::class,
            'blog_user',
            'blog_id',
            'user_id');
    }

    //Check if user has a role
    public function hasAnyRole(string $role)
    {
        return null!==$this->role()->where('id', $role)->first();
    }

    //Check user has any given role
    public function hasAnyRoles(array $role)
    {
        return null!==$this->role()->whereIn('id', $role)->first();
    }

    public static function walksAvailablePerUser(User $user)
    {
        $totalWalksQuery = DB::table('product_user')
            ->where('user_id', '=', $user->id)
            ->selectRaw(('total - used AS total'))
            ->get();

        return $totalWalksQuery->sum('total');
    }

    public static function dogsFromUser(User $user)
    {
        return DB::table('dogs')
            ->where('user_id', '=', $user->id)
            ->get();
    }

    public static function getNewUser($firstname, $lastname, $email)
    {
        return DB::table('users')
            ->select('id')
            ->where('firstname', '=', $firstname)
            ->where('lastname', '=', $lastname)
            ->where('email', '=', $email)
            ->get();
    }

}
