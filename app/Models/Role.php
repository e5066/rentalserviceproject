<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use HasFactory;


    protected $fillable = [
        'name'
    ];

    public function users()
    {
        return $this->hasMany(
            User::class
        );
    }

    public static function isAdmin($user_id)
    {
        $currentRole = DB::table('users')
            ->select('role_id')
            ->where('id', '=', $user_id)
            ->get();

        if ($currentRole[0]->role_id == 1){
            return false;
        }
        else{
            return true;
        }
    }




}
