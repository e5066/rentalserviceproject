<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class Admin extends Model
{
    use HasFactory;

    public static function getCustomerList(){
        return
            DB::table('users')
                ->select('id')
                ->addSelect('firstname')
                ->addSelect('lastname')
                ->addSelect('email')
                ->where('users.role_id', '=', 1)
                ->get();
    }

    public static function getCustomerInfo($userId){
        DB::statement("SET SQL_MODE=''");
        return DB::table('product_user')
            ->select('products.name')
            ->addSelect('products.price')
            ->selectRaw('SUM(product_user.total) as total')
            ->selectRaw("SUM(product_user.used) as used")
            ->addSelect("products.description")
            ->addSelect('product_id')
            ->selectRaw("COUNT(product_id) as productCount")
            ->where('product_user.user_id', '=', $userId)
            ->join('products', 'product_user.product_id', '=', 'products.id')
            ->groupBy('product_user.product_id')
            ->get();
    }

    public static function getCustomerDogs($userId){
        return Dog::where('dogs.user_id', '=', $userId)->get();
    }

    public static function getTotalProductsBought()
    {
        DB::statement("SET SQL_MODE=''");
        $cards = DB::table('products')
            ->selectRaw("COUNT(product_user.user_id) AS total")
            ->addSelect('products.name')
            ->addSelect('product_user.created_at')
            ->addSelect('products.id')
            ->leftJoin('product_user', 'product_user.product_id', '=', 'products.id')
            ->orderBy('products.walkcount')
            ->groupBy('products.id')
            ->get();

        $yearlySales = DB::table('product_user')
            ->selectRaw("DISTINCT products.name,
                COUNT(product_user.product_id) AS total,
                date_format(convert(product_user.created_at, DATE), '%Y') AS year")
            ->join('products', 'product_user.product_id', '=', 'products.id')
            ->whereRaw("date_format(convert(product_user.created_at, DATE), '%Y') = " . date("Y"))
            ->groupByRaw("date_format(CONVERT(product_user.created_at, DATE), '%Y'),
                product_id")
            ->orderByRaw("COUNT(product_user.product_id) DESC")
            ->get();

        $totalProfits = DB::table('product_user')
            ->selectRaw("products.name, COUNT(product_user.product_id) as total, products.price")
            ->join('products', 'product_user.product_id', '=', 'products.id')
            ->groupByRaw('product_user.product_id, products.price')
            ->orderBy('products.price')
            ->get();

        return [$cards[2], $cards[1] ,$cards[0], $yearlySales, $totalProfits];
    }

    public static function getEmployeesList()
    {
        return
            DB::table('users')
                ->select('users.id')
                ->addSelect('firstname')
                ->addSelect('lastname')
                ->addSelect('email')
                ->selectRaw('roles.name as roleName')
                ->where('users.role_id', '!=', 1)
                ->join('roles', 'users.role_id', '=', 'roles.id')
                ->get();
    }

    public static function getJobList()
    {
        return
            DB::table('roles')
                ->select('id')
                ->addSelect('name')
                ->get();
    }

    public static function getAllPeopleInDB()
    {
        return User::all();
    }

    public static function getCurrentEmployee($employee)
    {
        $currentEmployee = User::find($employee);
        $currentRole = DB::table('roles')
                        ->select('name')
                        ->addSelect('id')
                        ->where('id', '=', $currentEmployee->role_id)
                        ->get();

        return [$currentEmployee, $currentRole];
    }

    public static function getProductList(){
        return
            DB::table('products')
                ->select('id')
                ->addSelect('name')
                ->addSelect('walkcount')
                ->addSelect('price')
                ->addSelect('description')
                ->addSelect('img')
                ->get();


    }

    public static function getDogs(){
        return
            DB::table('dogs')
                ->select('id')
                ->addSelect('name')
                ->addSelect('description')
                ->addSelect('age')
                ->addSelect('allergies')
                ->addSelect('user_id')
                //->join('users', 'dogs.user_id', '=', 'users.id')
                ->get();


    }

    public static function getCustomerEmail($user_id)
    {
        return User::where('id', '=', $user_id)->get();
    }

    public static function getApprovalList(){
        return
            DB::table('dog_walk')
                ->select('dog_walk.approve')
                ->addSelect('dog_walk.dog_id')
                ->addSelect('walks.id')
                ->addSelect('timeslots.time')
                ->addSelect('walks.date')
                ->addSelect('users.firstname')
                ->addSelect('dogs.name')
                ->addSelect('users.email')
                ->addSelect('dog_walk.status')
                ->whereIn('dog_walk.approve', [0, 1])
                ->join('walks', 'dog_walk.walk_id', '=', 'walks.id')
                ->join('timeslots', 'walks.timeslot_id', '=', 'timeslots.id')
                ->join('dogs', 'dog_walk.dog_id', '=', 'dogs.id')
                ->join('users', 'dogs.user_id', '=', 'users.id')
                ->get();


    }

    public static function selectedWalk($id)
    {
        return
            DB::table('dog_walk')
                ->selectRaw('*')
                ->where('dog_walk.walk_id', '=', $id)
                ->get();
    }

    public static function updateCurrentWalk($id, $approve, $dog, $status)
    {
        DB::table('dog_walk')
            ->where([
                ['dog_walk.walk_id', '=', $id],
                ['dog_walk.dog_id', '=', $dog],
                ])
            ->update(['approve' => $approve, 'status' => $status]);
    }

    public static function isAdmin()
    {
        if(Auth::id()==null){
            return false;
        }
        else{
            $isAdmin = DB::table('users')
                ->select('role_id')
                ->where('users.id', '=', Auth::id())
                ->get()[0];
            if ($isAdmin->role_id != 1){
                return true;
            }
            else{
                return false;
            }
        }
    }
}

