<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Walk extends Model
{
    use HasFactory;

    protected $fillable = [
        'date',
    ];

    public function timeslot()
    {
        return $this->belongsTo(Timeslot::class);
    }

    public function dogs()
    {
        return $this->belongsToMany(
            Dog::class,
            'dog_walk',
            'dog_id',
            'walk_id');
    }
}
