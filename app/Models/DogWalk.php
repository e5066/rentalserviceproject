<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DogWalk extends Model
{
    protected $table = 'dog_walk';
    use HasFactory;

    protected $fillable = [
        'time',
        'timeslot_id'
    ];
}
