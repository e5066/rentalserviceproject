<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;

class CustomerAdminController extends Controller
{
    public function index(){
        abort_if(!Admin::isAdmin(), 403);
        $data = ['customers' => Admin::getCustomerList()];
        return view('admin.admin_customer', ['data' => $data]);

    }

    public function show($userId){
        abort_if(!Admin::isAdmin(), 403);
        $data = [
            'user' => User::find($userId),
            'boughtProducts' => Admin::getCustomerInfo($userId),
            'customerDogs' => Admin::getCustomerDogs($userId)
        ];
        return view('admin.admin_current_customer', ['data' => $data]);
    }
}
