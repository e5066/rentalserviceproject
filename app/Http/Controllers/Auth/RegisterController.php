<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Dog;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use http\Env\Response;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data['requestUser'], [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        try {
            $user=User::create([
                'name' => $data['requestUser']['firstname'],
                'firstname' => $data['requestUser']['firstname'],
                'lastname' => $data['requestUser']['lastname'],
                'street' => $data['requestUser']['street'],
                'home_no'=>$data['requestUser']['home_no'],
                'zipcode'=>$data['requestUser']['zipcode'],
                'email' => $data['requestUser']['email'],
                'password' => Hash::make($data['requestUser']['password']),
                'role_id'=>1,

            ]);
            $user->save();
            $user->refresh();

            $keys = array();
            $allKeys = array();
            foreach ($data['requestDog'] as $key => $value){
                if (count($keys) >= 4){
                    array_push($keys, $key);
                    array_push($allKeys, $keys);
                    $keys = array();
                }
                else{
                    array_push($keys, $key);
                }
            }

            foreach ($allKeys as $dog){
                Dog::create([
                    'name' =>$data['requestDog'][$dog[0]],
                    'age' =>$data['requestDog'][$dog[1]],
                    'dog_pic' => $data['requestDog'][$dog[2]],
                    'allergies' =>$data['requestDog'][$dog[3]],
                    'description' =>$data['requestDog'][$dog[4]],
                    'user_id' => User::getNewUser($data['requestUser']['firstname'], $data['requestUser']['lastname'], $data['requestUser']['email'])[0]->id,
                    'permissionPhotos' => 0,
                ]);
            }

            return $user;

        }catch(\Exception $ex){
            dd($ex);
        }
    }
}
