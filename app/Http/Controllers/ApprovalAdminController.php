<?php

namespace App\Http\Controllers;
use App\Mail\WalkDenyMail;
use App\Models\Admin;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ApprovalAdminController extends Controller
{
    public function index()
    {
        abort_if(!Admin::isAdmin(), 403);
        $data = [
            'approvedWalks' => Admin::getApprovalList()
        ];
        return view('admin.admin_approval', ['data' => $data]);
    }

    public function edit($id){
        abort_if(!Admin::isAdmin(), 403);
        $approvedWalks = Admin::selectedWalk($id);
        return view('admin.admin_approval_edit', ['approvedWalks' => $approvedWalks]);
    }

    public function update($id, $dog){
        abort_if(!Admin::isAdmin(), 403);
        Admin::updateCurrentWalk($id, 1, $dog, 1);
        return response()->json(['response' => 'Goedgekeurd']);
    }

    public function destroy($walk){
        abort_if(!Admin::isAdmin(), 403);
        $walkProperties = explode(',', $walk);
        $details = ['firstname' => $walkProperties[0], 'dogName' => $walkProperties[1], 'timeSlot' => $walkProperties[2], 'walkDate' => $walkProperties[3], 'email' => $walkProperties[4], 'dogId' => $walkProperties[5], 'walkId' => $walkProperties[6]];
        Mail::to($details['email'])->send(new WalkDenyMail($details));
        Admin::updateCurrentWalk($details['walkId'], 0, $details['dogId'], 2);
        return response()->json(['response' => 'Geannuleerd']);
    }

    public function show(){
        abort_if(!Admin::isAdmin(), 403);
        return response()->json(['list' => Admin::getApprovalList()]);
    }


}
