<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Models\product;
use App\Http\Requests\StoreproductsRequest;
use App\Http\Requests\UpdateproductsRequest;
use App\Models\ProductUser;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Mollie\Laravel\Facades\Mollie;
use Symfony\Component\HttpFoundation\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('webshop', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreproductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreproductsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\product  $products
     * @return \Illuminate\Http\Response
     */
    public function show(product $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\product  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(product $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateproductsRequest  $request
     * @param  \App\Models\product  $products
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateproductsRequest $request, product $products)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\product  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(product $products)
    {
        //
    }

    public function checkout(product $product) {
        return view('checkout', ['product' => $product]);
    }



    public function finalizeCheckout(product $product)
    {
        if (Auth::check() === false) {
            return redirect('/');
        }

        $user = Auth::user();

        $productUser = new ProductUser();
        $productUser->user_id = $user->id;
        $productUser->product_id = $product->id;
        $productUser->total = $product->walkcount;
        $productUser->used = 0;
        $productUser->price = $product->price;
        $productUser->status = 'open';

        $productUser->save();

        $orderId = $productUser->id;
        $data = [
          'value' => number_format($product->price, 2),
          'description' => 'Order' . $orderId,
            'orderId' => $orderId
        ];

        $payment = $this->preparePayment($data);
        return redirect($payment->getCheckoutUrl(), 303);
    }

    public function returnPendingView() {
        return view('succes');
    }

    public function checkStatus($id)
    {
        $productUser = ProductUser::findOrFail($id);

        $data = [
            'status' => $productUser->status
        ];

        return new JsonResponse($data);
    }

    private function preparePayment($data)
    {
        $payment = Mollie::api()->payments->create([
            "amount" => [
                "currency" => "EUR",
                "value" => $data['value'] // You must send the correct number of decimals, thus we enforce the use of strings
            ],
            "description" => $data['description'],
            "redirectUrl" => route('pending', ['id' => $data['orderId']]),
//            "webhookUrl" => route('webhooks.mollie'),
            "metadata" => [
                "order_id" => $data['orderId'],
            ],
        ]);

        return $payment;

        // redirect customer to Mollie checkout page
    }

    private function handle(Request $request) {
        if (! $request->has('id')) {
            return;
        }

        $payment = Mollie::api()->payments()->get($request->id);

        $id = $payment->orderId;

        $productUser = ProductUser::findOrFail($id);
        $productUser->status = $payment->status;
        $productUser->save();

        return new JsonResponse();
    }
}
