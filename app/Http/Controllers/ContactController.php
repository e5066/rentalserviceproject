<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('contact');
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|max:50',
            'phone' => 'required|numeric',
            'subject' => 'required|max:50',
            'msg' => 'required|string|min:3|max:1000'
        ]);

        $details =[
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'subject' => $request->subject,
            'msg' => $request->msg

        ];

        Mail::to("a3hondservice@gmail.com")->send(new ContactMail($details));
        //return view('admin.admin_recap');
        return back()->with('message_sent' , 'Uw bericht is verzonden. Wij nemen spoedig contact met u op!');

    }
}
