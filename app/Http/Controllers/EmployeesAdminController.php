<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeesAdminController extends Controller
{
    public function index()
    {
        abort_if(!Admin::isAdmin(), 403);
        $data = [
            'employees' => Admin::getEmployeesList()
        ];
        return view('admin.admin_employees', ['data' => $data]);
    }

    public function create()
    {
        abort_if(!Admin::isAdmin(), 403);
        $data = [
            'jobList' => Admin::getJobList(),
            'everybody' => Admin::getAllPeopleInDB(),
            'employees' => Admin::getEmployeesList()
        ];
        return view('admin.admin_employee_add', ['data' => $data]);
    }

    public function store(Request $request)
    {
        abort_if(!Admin::isAdmin(), 403);
        $selectedUser = User::find($request->user);
        $selectedUser->role_id = $request->employeeJob;
        $selectedUser->save();
        $selectedUser->refresh();
        return back()->with('success', "De rechten van: " . $selectedUser->firstname . " zijn geüpdate.");
    }

    public function edit($employee){
        abort_if(!Admin::isAdmin(), 403);
        $data = [
            'employee' => Admin::getCurrentEmployee($employee),
            'jobList' => Admin::getJobList()
        ];
        return view('admin.admin_employee_edit', ['data' => $data]);
    }

    public function update(Request $request, $employee){
        abort_if(!Admin::isAdmin(), 403);
        $currentEmployee = User::find($employee);
        $currentEmployee->email = $request->email;
        $currentEmployee->role_id = $request->employeeJob;
        $currentEmployee->save();
        $currentEmployee->refresh();
        return back()->with('success', "Account van: " . $currentEmployee->name . " is gewijzigd.");
    }

    public function destroy(User $employee){
        abort_if(!Admin::isAdmin(), 403);
        $oldName = $employee->name;
        DB::table('users')->where('id', '=', $employee->id)->delete();
        return redirect('/admin/employees')->with('success', $oldName . " is verwijderd uit het systeem.");
    }
}
