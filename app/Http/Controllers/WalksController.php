<?php

namespace App\Http\Controllers;

use App\Models\DogWalk;
use App\Models\Timeslot;
use App\Models\User;
use App\Models\walk;
use App\Http\Requests\StorewalksRequest;
use App\Http\Requests\UpdatewalksRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Js;
use Symfony\Component\HttpFoundation\Request;

class WalksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('create_walk');
    }

    public function getWalksPerMonth()
    {
        $month = new \DateTime('now');
        $stringMonth = $month->format('Y-m-d');
        return $this->walksPerMonth($stringMonth);
    }

    public function getWalksAvailablePerUser()
    {
        $user = Auth::user();
        return User::walksAvailablePerUser($user);
    }

    public function getTimeSlots()
    {
        $timeslots = Timeslot::all();

        $data = [];
        foreach ($timeslots as $timeslot) {
            $datetime = new \DateTime($timeslot->time);
            $datetimeModified = new \DateTime($timeslot->time);
            $data[] = [
                'label' => $datetime->format('H:i') . ' - ' . $datetimeModified->modify('+1 hour')->format('H:i') ,
                'value' => $datetime->format('H:i')
            ];
        }

        return $data;
    }

    public function walksPerMonth($month)
    {
        $yearMonth = substr($month, 5);
        $formattedMonth = substr($yearMonth, -5 ,2);
        $walksPerDay = DB::table('dog_walk')
            ->leftJoin('walks', 'dog_walk.walk_id', '=', 'walks.id')
            ->whereMonth('date', '=', $formattedMonth)
            ->groupBy('date', 'walks.timeslot_id')
            ->selectRaw('date, walks.timeslot_id ,COUNT(dog_id) as total')
            ->get();

        $timeslots = Timeslot::all();

        for ($i = 0; $i < 30; $i++) {
           $dates[] = new \DateTime('+'. $i .' day');
        }
        $dateArray = [];
        foreach ($dates as $date) {

            $dateItem = [
                'date' => $date->format('Y-m-d'),
                'timeslots' => []
            ];

            foreach ($timeslots as $timeslot) {
                $dateItem['timeslots'][substr($timeslot->time, 0, -3)] = 0;
            }


            foreach ($timeslots as $timeslot) {
                foreach ($walksPerDay as $walk) {
                    if ($walk->timeslot_id === $timeslot->id && $walk->date === $date->format('Y-m-d')) {
                        $dateItem['timeslots'][substr($timeslot->time, 0, -3)] = $walk->total;
                    }
                }
            }



            $dateArray[] = $dateItem;
        }

        return $dateArray;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $allowed = false;
        $data = json_decode($request->getContent());

        $timeslot = DB::table('timeslots')->where('time', '=', $data->time)->first();

        $yearMonth = substr($data->date, 5);
        $month = substr($yearMonth, -5 ,2);

        $walksPerMonth = $this->walksPerMonth($month);

        $amountofDogs = count($data->dogs);

        foreach ($walksPerMonth as $walkPerDay) {

            if ($walkPerDay['date'] === $data->date ) {

                foreach ($walkPerDay['timeslots'] as $key => $value) {
                    if ($key === $data->time) {
                        if ($value + $amountofDogs <= 8) {
                            $allowed = true;
                        }
                    }
                }
            }
        }

        if ($allowed === true) {
            $walk = new Walk();
            $walk->date = new \DateTime($data->date);
            $walk->timeslot_id = $timeslot->id;
            $walk->save();
            foreach ($data->dogs as $dog) {
                $dogWalk = new DogWalk();
                $dogWalk->approve = 0;
                $dogWalk->walk_id = $walk->id;
                $dogWalk->dog_id = $dog;
                $dogWalk->save();
            }
            return new JsonResponse(['status' => 'success']);
        }
        return new JsonResponse(['status' => 'failed']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorewalksRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorewalksRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\walk  $walks
     * @return \Illuminate\Http\Response
     */
    public function show(walk $walks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\walk  $walks
     * @return \Illuminate\Http\Response
     */
    public function edit(walk $walks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatewalksRequest  $request
     * @param  \App\Models\walk  $walks
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatewalksRequest $request, walk $walks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\walk  $walks
     * @return \Illuminate\Http\Response
     */
    public function destroy(walk $walks)
    {
        //
    }
}
