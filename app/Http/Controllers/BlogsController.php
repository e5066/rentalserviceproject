<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\blog;
use App\Http\Requests\StoreblogsRequest;
use App\Http\Requests\UpdateblogsRequest;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::getAllBlogs(0);
        foreach ($blogs as $key => $blog){
            $blogs[$key]['liked'] = Blog::checkLike($blog->id);
            $blogs[$key]['likeCount'] = Blog::countLikes($blog->id)[0]->likesCount;
            switch (strlen((string)$blog->likeCount)){
                case 4:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . "." . $splitLikes[1] . $splitLikes[2] . "K";
                    break;
                case 5:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . $splitLikes[1] . "." . $splitLikes[2] . $splitLikes[3] . "K";
                    break;
                case 6:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . $splitLikes[1] . $splitLikes[2] . "." . $splitLikes[3] . $splitLikes[4] . "K";
                    break;
                case 7:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . "." . $splitLikes[1] . $splitLikes[2] . "M";
                    break;
                case 8:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . $splitLikes[1] . "." . $splitLikes[2] . $splitLikes[3] . "M";
                    break;
                case 9:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . $splitLikes[1] . $splitLikes[2] . "." . $splitLikes[3] . $splitLikes[4] . "M";
                    break;
                case 10:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . "." . $splitLikes[1] . $splitLikes[2] . "B";
                    break;
                case 11:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . $splitLikes[1] . "." . $splitLikes[2] . $splitLikes[3] . "B";
                    break;
                case 12:
                    $splitLikes = str_split($blog->likeCount, 1);
                    $blog->likes = $splitLikes[0] . $splitLikes[1] . $splitLikes[2] . "." . $splitLikes[3] . $splitLikes[4] . "B";
                    break;
                default:
                    break;
            }
        }
        return view('blog.blog_home', ['pagination' => $blogs]);
    }

    public function allBlogs($currentPage)
    {
        $blogs = Blog::getAllBlogs($currentPage);
        foreach ($blogs as $key => $blog){
            $blog->photo = 'data:image;base64,' . base64_encode($blog->photo);
            $blogs[$key]['liked'] = Blog::checkLike($blog->id);
            $blogs[$key]['likeCount'] = Blog::countLikes($blog->id)[0]->likesCount;
        }
        return response()->json(["blogs" => $blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreblogsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreblogsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\blog  $blogs
     * @return \Illuminate\Http\Response
     */
    public function show(blog $blogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\blog  $blogs
     * @return \Illuminate\Http\Response
     */
    public function edit(blog $blogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateblogsRequest  $request
     * @param  \App\Models\blog  $blogs
     * @return \Illuminate\Http\Response
     */
    public function update($blog_id)
    {
        Blog::addLike($blog_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\blog  $blogs
     * @return \Illuminate\Http\Response
     */
    public function destroy($blog)
    {
        Blog::removeLike($blog);
    }
}
