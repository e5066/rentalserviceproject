<?php

namespace App\Http\Controllers;

use App\Models\dog;
use App\Http\Requests\StoredogsRequest;
use App\Http\Requests\UpdatedogsRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dogs.dog-show',['dogs'=>Dog::orderBy('id','Asc')->get()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dogs.dogs');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoredogsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoredogsRequest $request)
    {
        $dog= new Dog();
        $dog->user_id=request('user_id');
        $dog->name=request('name');
        $dog->age=request('age');
        $dog->allergies=request('allergies');
        $dog->dog_pic=request('dog_pic');
        $dog->description=request('description');
        $dog->permissionPhotos=request('permissionPhotos');
        $dog->save();
        return redirect('/dogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\dog  $dogs
     * @return \Illuminate\Http\Response
     */
    public function show(dog $dogs)
    {
        $dogs=Dog::find($dogs);
        return view('dogs.dog-show',compact('dogs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\dog  $dogs
     * @return \Illuminate\Http\Response
     */
    public function edit(dog $dogs)
    {
        $dogs=Dog::find($dogs);
        return view('dogs.dog-edit',compact('dogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatedogsRequest  $request
     * @param  \App\Models\dog  $dogs
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatedogsRequest $request, dog $dogs)
    {
        $dogs=Dog::find($dogs->id);
        $dogs->name=$request->name;
        $dogs->user_id=$request->user_id;
        $dogs->permissionPhotos->$request->permissionPhotos;
        $dogs->dog_pic=$request->dog_pic;
        $dogs->description=$request->description;
        $dogs->age=$request->age;
        $dogs->allergies=$request->allergies;
        $dogs->save();
        return redirect('/dogs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\dog  $dogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(dog $dogs)
    {
        $dogs=Dog::find($dogs->id)->delete();
        return redirect('/dogs');
    }
    public static function getTempDogs(){
        return DB::table('dogs')
            ->select('id','name','age','description','allergies','user_id','permissionPhotos')
            ->get();
    }

    public function dogsFromUser()
    {
        $user = Auth::user();
        return User::dogsFromUser($user);
    }
}
