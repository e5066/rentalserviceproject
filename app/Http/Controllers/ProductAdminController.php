<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Requests\StoreproductsRequest;
use App\Models\User;
use App\Models\Model;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ProductAdminController extends Controller
{


    public function index(){
        abort_if(!Admin::isAdmin(), 403);
        $data = ['products' => Admin::getProductList()];
        return view('admin.admin_product', ['data' => $data]);

    }

    public function create(){
        abort_if(!Admin::isAdmin(), 403);

        return view('admin.admin_product_create');
    }

    public function store(Request $request){
        abort_if(!Admin::isAdmin(), 403);

        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'walkcount' => 'required|numeric',
            'description' => 'required|string|min:3|max:1000',

        ]);

         $data = new Product();
         $data->name = $request->name;
         $data->price = $request->price;
         $data->walkcount = $request->walkcount;
         $data->description = $request->description;
         if($request->hasfile('image'))
         {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/product/', $filename);
            $data->img = $filename;
         }

        //dd($request->all());
        $data->save();

        return redirect()->route('product.look');

    }



    public function show($id){
        abort_if(!Admin::isAdmin(), 403);
        $product = Product::find($id);
        return view('admin.admin_product_edit', ['product' => $product]);
    }

    public function edit($id){
        abort_if(!Admin::isAdmin(), 403);
        $product = Product::find($id);
        return view('admin.admin_product_edit', ['product' => $product]);
    }

    public function update(Request $request, $id){
        abort_if(!Admin::isAdmin(), 403);

        $this->validate($request,[
            'name' => 'required',
            'price' => 'required|numeric',
            'walkcount' => 'required|numeric',
            'description' => 'required|string|min:3|max:1000',

        ]);

        $data = Product::find($id);
        $data->name = $request->input('name');
        $data->price = $request->input('price');
        $data->walkcount = $request->input('walkcount');
        $data->description = $request->input('description');

        if($request->hasfile('image'))
        {
            $destination = 'uploads/product/'.$data->img;
            if(File::exists($destination))
            {
                File::delete($destination);
            }
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/product/', $filename);
            $data->img = $filename;
        }


        $data->update();

        return redirect()->route('product.look');
    }


    public function destroy($id)
    {
        abort_if(!Admin::isAdmin(), 403);
        $data=Product::find($id);
        $data->delete();
        return redirect()->route('product.look');

    }



}
