<?php

namespace App\Http\Controllers;

use App\Models\Dog;
use App\Models\user;
use App\Http\Requests\StoreuserRequest;
use App\Http\Requests\UpdateuserRequest;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($profile)
    {
        //
    }

    public function getInfoOfID($id){
        return DB::table('users')
            ->where('id', '=', $id)
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function createdog($profile)
    {
        $profile = $this->getInfoOfID($profile);
        return view('profile.dogs.dogs_create', ['profile' => $profile]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreuserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreuserRequest $request)
    {
        //
    }

    public function storedog(StoreuserRequest $request)
    {
        $dog = new Dog();

        $dog->name = $request->name;
        $dog->user_id = $request->ownerid;
        $dog->permissionPhotos = $request->blogallow;

        $dog->save();

        return redirect('profile/'.$request->ownerid.'/honden');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show($profile)
    {
        $profile = $this->getInfoOfID($profile);
        return view('profile.profile_view', ['profile' => $profile]);
        //
    }

    public function showdogs($profile)
    {
        $dogs = $this->getDogsOfID($profile);
        return view('profile.dogs.dogs_view', ['profile' => $profile, 'dogs' => $dogs]);
        //
    }

    public function showkaarten($profile)
    {
        $kaarten = $this->getProductsOfProfileID($profile);
        $products = $this->getProductInfo();
        return view('profile.kaarten.kaarten_view', ['profile' => $profile, 'products' => $products, 'kaarten' => $kaarten]);
        //
    }

    public function getProductInfo(){
        return DB::table('products')
            ->get();
    }

    public function getProductsOfProfileID($id){
        return DB::table('product_user')
            ->where('user_id', '=', $id)
            ->whereRaw('total <> used')
            ->join('products', 'product_id', '=', 'products.id')
            ->get();
    }

    public function getDogsOfID($id){
        return DB::table('dogs')
            ->where('user_id', '=', $id)
            ->get();
    }


    public function getInfoOfDog($id){
        return DB::table('dogs')
            ->where('id', '=', $id)
            ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($profile)
    {
        $profile = $this->getInfoOfID($profile);
        return view('profile.profile_edit', ['profile' => $profile]);
        //
        //
    }

    public function editpassword($profile)
    {
        $profile = $this->getInfoOfID($profile);
        return view('profile.password.password_edit', ['profile' => $profile]);
        //
        //
    }

    public function editdog($profile, $dog)
    {
        $profile = $this->getInfoOfID($profile);
        $dog = $this->getInfoOfDog($dog);
        return view('profile.dogs.dogs_edit', ['profile' => $profile, 'dog' => $dog]);
        //
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateuserRequest  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateuserRequest $request, user $user)
    {
        DB::table('users')
            ->where('id', $request->updid)
            ->update(['firstname' => $request->firstname, 'lastname' => $request->lastname, 'email' => $request->email]);


        return redirect('profile/'.$request->updid);
        //
    }

    public function updatepassword(UpdateuserRequest $request, user $user)
    {
        DB::table('users')
            ->where('id', $request->updid)
            ->update(['password' => $request->password]);

        return redirect('profile/'.$request->updid);
        //
    }

    public function updatedog(UpdateuserRequest $request, user $dog)
    {
        DB::table('dogs')
            ->where('id', $request->updid)
            ->update(['name' => $request->name, 'permissionPhotos' => $request->blogallow]);

        return redirect('profile/'.$request->retid . '/honden');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroydog($profile, $dog)
    {
        $doginfo = $this->getInfoOfDog($dog);

        DB::table('dogs')
            ->where('id', $dog)
            ->delete();

        return redirect('profile/' . $doginfo[0]->user_id . '/honden');
        //
    }
}
