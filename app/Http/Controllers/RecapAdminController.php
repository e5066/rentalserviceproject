<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Mail\RecapMail;
use Illuminate\Support\Facades\Mail;


class RecapAdminController extends Controller
{
    public function index(){
        abort_if(!Admin::isAdmin(), 403);
        $data = ['dogs' => Admin::getDogs()];
        return view('admin.admin_recap', ['data' => $data]);

    }

    /*
    public function sendEmail()
    {
        $details =[
            'title'=>'Mail from recap hondenuitlaatservice.',
            'body' =>'This is for testing purpose using gmail.'
        ];

        Mail::to("a3hondservice@gmail.com")->send(new RecapMail($details));
        //return view('admin.admin_recap');
        return "Email Sent";
    }
*/

    public function sendEmail(Request $request)
    {
        abort_if(!Admin::isAdmin(), 403);
        $test = array();
        $dogNames = "";
       foreach ($request->input() as $input){
           array_push($test, $input);
       }
        for ($i = 2; $i < count($test) - 1; $i++){
            $split = explode(',', $test[$i]);
            $split2 = explode(':' ,$split[1]);
            $dogNames = $split2[1] . ', ' . $dogNames;
        }

        $request->validate([
            'msg' => 'required|string|min:3|max:1000'
        ]);

        $details = [
            'msg' => $request->msg,
            'selectedDog' => $dogNames

        ];
        Mail::to(Admin::getCustomerEmail(json_decode($request->selectedDog)->user_id))->send(new RecapMail($details));
        //return view('admin.admin_recap');
        return back()->with('recap_sent' , 'De recap van de wandeling is succesvol naar de klant verzonden!');

    }



}
