let paginate = 1;

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
        loadMoreData($('#post').children().length);
    }
});

// run function when user click load more button
function loadMoreData(paginate) {
    $.ajax({
        url: '/blog/' + paginate,
        type: 'get',
        datatype: 'json',
        beforeSend: function() {
            $('.spinner-border').css('visibility','visible');
        }
    })
        .done(function(data) {
            if(data.blogs.length == 0) {
                $('.spinner-border').hide();
                return;
            } else {
                $('.spinner-border').css('visibility','visible');
                data.blogs.forEach(blog=> $('#post').append(createBlog(blog)));
            }
        })
        .fail(function(jqXHR, ajaxOptions, thrownError) {
            alert('Kon de blogs niet ophalen, probeer het later nog een keer.');
            $('.spinner-border').hide();
        });
}

function createBlog(blog){
    const blogItem = document.createElement('div');
    const blogBody = document.createElement('div');

    //Add the image as card body first item in the card blogItem.
    const photo = document.createElement('img');

    //First row with all the basic content: img, likes, heartIcon and date.
    const blogBodyRow = document.createElement('div');

    //Second row with the blog description.
    const blogBodyRow2 = document.createElement('div');

    //Create the frist column in the row that contains the likes in number.
    const blogBodyCol1 = document.createElement('div');
    const blogBodyCol1Content = document.createElement('p');

    //Create the second column in the row that contains the heart icon.
    const blogBodyCol2 = document.createElement('div');
    const blogBodyCol2Content = document.createElement('i');

    //Create the third column in the row that contains the date.
    const blogBodyCol3 = document.createElement('div');
    const blogBodyCol3Content = document.createElement('p');

    //Create the first column for row2 that contains the description.
    const blogBodyCol4 = document.createElement('div');
    const blogBodyCol4Content = document.createElement('p');

    blogItem.className = 'card mb-5 mt-3 blog-card';
    blogBody.className = 'card-body px-0 pt-0';
    blogItem.append(blogBody);

    photo.className = 'img-fluid';
    photo.style.width = "100%";
    photo.src = blog.photo;
    blogBody.append(photo);

    blogBodyRow.className = 'row align-items-center justify-content-start px-3 pt-1';
    blogBody.append(blogBodyRow);

    blogBodyRow2.className = 'row px-3 justify-content-start';
    blogBody.append(blogBodyRow2);

    blogBodyCol1.className = 'col-auto align-self-center my-auto pe-2 ps-0';
    blogBodyRow.append(blogBodyCol1);
    blogBodyCol1Content.className = 'mb-1';
    blogBodyCol1Content.id = blog.id + 'lc' + blog.likeCount;
    //CONVERT LIKES TO DECIMAL IF MORE THAN 4 NUMBERS.
    let likes;
    let likesToArray;
    switch (blog.likeCount.toString().length){
        case 4:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + "." + likesToArray[1] + likesToArray[2] + "K";
            break;
        case 5:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + likesToArray[1] + "." + likesToArray[2] + likesToArray[3] + "K";
            break;
        case 6:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + likesToArray[1] + likesToArray[2] + "." + likesToArray[3] + likesToArray[4] + "K";
            break;
        case 7:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + "." + likesToArray[1] + likesToArray[2] + "M";
            break;
        case 8:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + likesToArray[1] + "." + likesToArray[2] + likesToArray[3] + "M";
            break;
        case 9:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + likesToArray[1] + likesToArray[2] + "." + likesToArray[3] + likesToArray[4] + "M";
            break;
        case 10:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + "." + likesToArray[1] + likesToArray[2] + "B";
            break;
        case 11:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + likesToArray[1] + "." + likesToArray[2] + likesToArray[3] + "B";
            break;
        case 12:
            likesToArray = blog.likeCount.toString().split("");
            likes = likesToArray[0] + likesToArray[1] + likesToArray[2] + "." + likesToArray[3] + likesToArray[4] + "B";
            break;
        default:
            likes = blog.likeCount;
    }
    //END OF SWITCH
    blogBodyCol1Content.innerHTML = likes;
    blogBodyCol1.append(blogBodyCol1Content);

    blogBodyCol2.className = 'col-auto align-self-center my-auto pe-2 ps-0';
    blogBodyCol2Content.id = blog.id;
    blogBodyRow.append(blogBodyCol2);
    if (blog.liked === 1){
        blogBodyCol2Content.className = 'bi-heart-fill';
        blogBodyCol2Content.style.color = "red";
    }
    else{
        blogBodyCol2Content.className = 'bi bi-heart';
    }
    blogBodyCol2Content.style.fontSize = "16px";
    blogBodyCol2.append(blogBodyCol2Content);

    blogBodyCol3.className = 'col-auto align-self-center my-auto pe-2 ps-0';
    blogBodyRow.append(blogBodyCol3);
    blogBodyCol3Content.className = 'mb-1 text-start';
    blogBodyCol3Content.innerHTML = blog.postDate;
    blogBodyCol3.append(blogBodyCol3Content);

    blogBodyCol4.className = 'col-12 p-0';
    blogBodyRow2.append(blogBodyCol4);
    blogBodyCol4Content.innerHTML = blog.text;
    blogBodyCol4.append(blogBodyCol4Content);

    photo.onclick = function (){
        likeClick(blogBodyCol2Content.id, blogBodyCol1Content.id);
    }

    return blogItem;
}

function likeClick(heartId, likeCounter){
    const heart = document.getElementById(heartId);
    if (heart.classList.contains('bi-heart')){
        heart.classList.remove('bi-heart');
        heart.classList.add('bi-heart-fill');
        heart.style.color = "red";
        addLike(heartId, likeCounter);
    }
    else {
        heart.classList.remove('bi-heart-fill');
        heart.classList.add('bi-heart');
        heart.style.color = "black";
        deleteLike(heartId, likeCounter);
    }

}

function addLike(blog_id, likeCounter){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/blog/update/' + blog_id,
        type: 'put',
        datatype: 'json',
        data: {'blog_id' : blog_id},
        beforeSend: function() {
            const like = document.getElementById(likeCounter);
            let currentCount = Number(like.innerHTML);
            like.innerHTML = currentCount + 1;
        }
    })
        .fail(function(jqXHR, ajaxOptions, thrownError) {
            const like = document.getElementById(likeCounter);
            let currentCount = Number(like.innerHTML);
            like.innerHTML = (currentCount - 1);
            alert('Kon post niet liken...');
        });
}

function deleteLike(blog_id, likeCounter){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/blog/delete/' + blog_id,
        type: 'delete',
        datatype: 'json',
        data: {'blog_id' : blog_id},
        beforeSend: function() {
            const like = document.getElementById(likeCounter);
            let currentCount = Number(like.innerHTML);
            like.innerHTML = (currentCount - 1);
        }
    })
        .fail(function(jqXHR, ajaxOptions, thrownError) {
            const like = document.getElementById(likeCounter);
            let currentCount = Number(like.innerHTML);
            like.innerHTML = (currentCount + 1);
            alert('Kon post niet disliken...');
        });
}


