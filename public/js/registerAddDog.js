let counter = 0;
function addDog(){
    const row1 = document.createElement('div');
    row1.className = "row justify-content-start";
    const col1 = document.createElement('div');
    col1.className = "col-12";

    const row2 = document.createElement('div');
    row2.className = "row justify-content-start";
    const col2 = document.createElement('div');
    col2.className = "col-12 col-md-3";
    const col3 = document.createElement('div');
    col3.className = "col-12 col-md-9";

    const row3 = document.createElement('div');
    row3.className = "row justify-content-start";
    const col4 = document.createElement('div');
    col4.className = "col-12";

    const row4 = document.createElement('div');
    row4.className = "row justify-content-start";
    const col5 = document.createElement('div');
    col5.className = "col-12";

    //Create form-floating1
    const formObject = document.createElement('div');
    formObject.className = "form-floating mb-3";

    //Create form-floating2
    const formObject2 = document.createElement('div');
    formObject2.className = "form-floating mb-3";

    //Create form-floating3
    const formObject3 = document.createElement('div');
    formObject3.className = "form-floating mb-3";

    //Create form-floating4
    const formObject4 = document.createElement('div');
    formObject4.className = "form-floating mb-3";

    //Create form-floating5
    const formObject5 = document.createElement('div');
    formObject5.className = "form-floating mb-3";

    //First input of dog-fields "Dogname"
    const dogNameInput = document.createElement('input');
    dogNameInput.id = "name" + counter;
    dogNameInput.type = "text";
    dogNameInput.placeholder = "Naam";
    dogNameInput.className = "border-success input-fields form-control";
    dogNameInput.name = "name" + counter;
    formObject.append(dogNameInput);

    const dogNameLabel = document.createElement('label');
    dogNameLabel.setAttribute('for', 'name' + counter);
    dogNameLabel.innerHTML = "Naam";
    formObject.append(dogNameLabel);

    //Second input of dog-fields "age"
    const dogAgeInput = document.createElement('input');
    dogAgeInput.id = "age" + counter;
    dogAgeInput.type = "number";
    dogAgeInput.placeholder = "Leeftijd";
    dogAgeInput.className = "border-success input-fields form-control";
    dogAgeInput.name = "age" + counter;
    formObject2.append(dogAgeInput);

    const dogAgeLabel = document.createElement('label');
    dogAgeLabel.setAttribute('for', 'age' + counter);
    dogAgeLabel.innerHTML = "Leeftijd";
    formObject2.append(dogAgeLabel);

    //Third input of dog-fields "Photo" appends formObject3 and col2
    const dogPhotoInput = document.createElement('input');
    dogPhotoInput.id = "photo" + counter;
    dogPhotoInput.type = "file";
    dogPhotoInput.placeholder = "Foto";
    dogPhotoInput.className = "border-success input-fields form-control";
    dogPhotoInput.name = "dog_pic" + counter;
    formObject3.append(dogPhotoInput);

    const dogPhotoLabel = document.createElement('label');
    dogPhotoLabel.setAttribute('for', 'photo' + counter);
    dogPhotoLabel.innerHTML = "Foto";
    formObject3.append(dogPhotoLabel);


    //Fourth input of dog-fields "Allergies"
    const dogAllergiesInput = document.createElement('input');
    dogAllergiesInput.id = "allergies" + counter;
    dogAllergiesInput.type = "text";
    dogAllergiesInput.placeholder = "Allergieën";
    dogAllergiesInput.className = "border-success input-fields form-control";
    dogAllergiesInput.name = "allergies" + counter;
    formObject4.append(dogAllergiesInput);

    const dogAllergiesLabel = document.createElement('label');
    dogAllergiesLabel.setAttribute('for', 'allergies' + counter);
    dogAllergiesLabel.innerHTML = "Allergieën";
    formObject4.append(dogAllergiesLabel);

    //Fifth input of dog-fields "Description"
    const dogDescriptionInput = document.createElement('textarea');
    dogDescriptionInput.id = "description" + counter;
    dogDescriptionInput.placeholder = "Weetjes";
    dogDescriptionInput.className = "border-success input-fields form-control";
    dogDescriptionInput.name = "description" + counter;
    formObject5.append(dogDescriptionInput);

    const dogDescriptionLabel = document.createElement('label');
    dogDescriptionLabel.setAttribute('for', 'description' + counter);
    dogDescriptionLabel.innerHTML = "Weetjes";
    formObject5.append(dogDescriptionLabel);

    col1.append(formObject);
    col2.append(formObject2);
    col3.append(formObject3);
    col4.append(formObject4);
    col5.append(formObject5);
    row1.append(col1);
    row2.append(col2);
    row2.append(col3);
    row3.append(col4);
    row4.append(col5);
    $('#dog-inputs').append(row1);
    $('#dog-inputs').append(row2);
    $('#dog-inputs').append(row3);
    $('#dog-inputs').append(row4);

    counter++;
}

function registerUser(){
    const requestDog = {};
    const requestUser = {};
    const parentDog = document.getElementById('dog-inputs').childNodes;
    const parentUser = document.getElementById('userFields').childNodes;
    for (let i = 0; i < parentDog.length; i++) {
        const cols = parentDog[i].childNodes; // childs of divs that contains inputs
        for (let k = 0; k < cols.length; k++) {
            const formControls = cols[k].childNodes;
            for (let y = 0; y < formControls.length; y++){
                const inputs = formControls[y].childNodes;
                for (let x = 0; x < inputs.length; x++){
                    const input = inputs[0]
                    console.log(input.name);
                    console.log("--------------------------------------------------------");
                    console.log(input.value);
                    requestDog[input.name] = input.value;
                    x++;
                }
            }
        }
    }
    for (let i = 0; i < parentUser.length; i++) {
        const cols = parentUser[i].childNodes; // childs of divs that contains inputs
        for (let k = 0; k < cols.length; k++) {
            const formControls = cols[k].childNodes;
            for (let y = 0; y < formControls.length; y++){
                const inputs = formControls[y].childNodes;
                for (let x = 0; x < inputs.length; x++){
                    const input = inputs[1]
                    console.log(input.name);
                    console.log("--------------------------------------------------------");
                    console.log(input.value);
                    requestUser[input.name] = input.value;
                    x = x + 4;
                }
            }
        }
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/register',
        type: 'POST',
        datatype: 'json',
        data: {'requestUser': requestUser, 'requestDog': requestDog},
        beforeSend: function() {
            console.log("AJAX REQUEST STARTED");
        }
    })
        .done(function(data) {
            console.log(data.data);
            if(data.data == null) {
                window.location.href = window.location.hostname + '/home';
                return;
            } else {
                window.location.href = window.location.hostname + '/home';
            }
        })
        .fail(function(jqXHR, ajaxOptions, thrownError) {
            console.log(jqXHR + " SOMETHING WENT WRONG!!");
        });
}
