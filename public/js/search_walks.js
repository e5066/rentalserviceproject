function search_walks() {
    var input, filter, table, tr, td1, i, txtValue;
    input = document.getElementById("walksInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("walksTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td1 = tr[i].getElementsByTagName("td")[0];
        if (td1) {
            txtValue = td1.textContent || td1.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
