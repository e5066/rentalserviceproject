window.addEventListener('resize', onresize);
function onresize(){
    if (screen.width > 992){
        var vnav = document.getElementById('vnav');
        vnav.classList.add('min-vh-100');
    }
    else{
        var vnav = document.getElementById('vnav');
        vnav.classList.remove('min-vh-100');
    }
}
window.onresize = onresize();
